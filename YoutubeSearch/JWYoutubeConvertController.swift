//
//  JWYoutubeConvertController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/30/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

//import Foundation
import UIKit

protocol JWYoutubeConvertDelegate: class {
    func convertController(_ controller:JWYoutubeConvertController, didObtainLink linkString:String) -> Void
    func convertControllerDidInitiateWebView(_ controller:JWYoutubeConvertController) -> Void
    func convertControllerDidNotInitiateWebView(_ controller:JWYoutubeConvertController) -> Void
    func convertControllerWebViewDidFinishFirstLoad(_ controller:JWYoutubeConvertController) -> Void
    func convertController(_ controller:JWYoutubeConvertController, didSaveFileDataForDbKey dbkey:String) -> Void
    func convertControllerDidRetrieveFile(_ controller:JWYoutubeConvertController) -> Void
    func convertControllerDoesConvert(_ controller:JWYoutubeConvertController) -> Void
    func convertControllerDoesFile(_ controller:JWYoutubeConvertController, dbKey dbkey:String) -> Void
    func convertControllerCoverImageAvailable(_ controller:JWYoutubeConvertController, coverImage image:UIImage) -> Void
    func convertController(_ controller:JWYoutubeConvertController, progress:Float) -> Void
}

extension JWYoutubeConvertDelegate {
    func convertController(_ controller:JWYoutubeConvertController, didObtainLink linkString:String) { }
    func convertControllerDidInitiateWebView(_ controller:JWYoutubeConvertController) { }
    func convertControllerDidNotInitiateWebView(_ controller:JWYoutubeConvertController) { }
    func convertControllerWebViewDidFinishFirstLoad(_ controller:JWYoutubeConvertController) { }
    func convertController(_ controller:JWYoutubeConvertController, didSaveFileDataForDbKey dbkey:String) { }
    func convertControllerDidRetrieveFile(_ controller:JWYoutubeConvertController) { }
}



enum ConverterState {
    case idle
    case converting
    case downloading
    case cancelled
}

class JWYoutubeConvertController: NSObject, JWYoutubeMP3DotOrgDelegate, UIWebViewDelegate {
    
    // MARK: Private

    fileprivate var urlSession: JWYoutubeMP3DotOrg?
    fileprivate weak var webView: UIWebView?
    fileprivate var loadCount = 0
    fileprivate var hasLink = false
    fileprivate var downloadController: JWFileDownloader?
    fileprivate var currentCacheItem: String?
    fileprivate var state: ConverterState = .idle
    fileprivate var imageRetrievalQueue: DispatchQueue
    fileprivate var progressDidObtainLink = false
    fileprivate let convertProgressOfTotal: Float = 0.35  // each progress of total must sum to equal 1
    fileprivate let downloadProgressOfTotal: Float  = 0.65
    
    // MARK: Public
    
    weak var delegate: JWYoutubeConvertDelegate?
    
    var dbkey: String?
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    var automatic = false

    override init() {
        urlSession = JWYoutubeMP3DotOrg()
        imageRetrievalQueue = DispatchQueue(label: "com.joker.imageProcessingConvert",attributes: DispatchQueue.Attributes.concurrent)
        super.init()
    }
    
    convenience init(delegate: JWYoutubeConvertDelegate?, webView:UIWebView?) {
        self.init()
        self.delegate = delegate
        if let initWebView = webView {
            initialzeConvertControllerWithWebView(initWebView)
        }
    }
    
    func initialzeConvertControllerWithWebView(_ webView:UIWebView) {
        self.webView = webView;
        loadCount = 0;
        hasLink = false
    }
    
    func prepareToBeginNewSessionWithLinkURL(_ linkURL:URL, forDbKey dbkey:String) {
        
        print("\(#function) \(linkURL.lastPathComponent) \(dbkey)")

        webView!.delegate = nil
        urlSession!.delegate = nil
        urlSession = nil
        self.dbkey = dbkey
        loadCount = 0;
        hasLink = false

        urlSession = JWYoutubeMP3DotOrg(replaceString:linkURL.absoluteString, delegate: self, dbkey: dbkey )

        // the urlSession does not get going until startWebSession
    }
    
    func startSession() {
        
        print("\(#function)")
        state = .converting
        webView!.delegate = self
        
        urlSession!.startWebSession()
    }
    
    func reconvert() {
        
        if let linkURLStr = urlSession!.youTubeLinkURLStr {
            state = .converting
            urlSession!.youTubeURLReplaceString = linkURLStr
            
            newSessionWithLinkURLString(linkURLStr, forDbKey:dbkey!)
        } else {
            print("no link URL available")
        }
    }
    
    
    /*
     Cancel will need to stop the conversion/download process
     Some states to be concerned with here are
     - if the conversion is in progress waiting for the download link
     - if the download is in progress cancel it
     - delicately handle in between
     
     */
    func cancel() {
        
        let fromState = state
        state = .cancelled
        delegate = nil
        
        if fromState == .converting {
            webView!.delegate = nil
            urlSession!.delegate = nil
        } else {
            if let downloader = downloadController {
                downloader.cancel()
            }
        }
        
    }
    
    
    // MARK: webview delegate

    func webViewDidFinishLoad(_ webView:UIWebView) {
    
        if state == .cancelled {
            return
        }
        
        loadCount += 1
        if loadCount == 1 {
            // First load begin convert
            urlSession!.submitConvert(webView)
            
            // Notify the delegate we are on our way
            delegate?.convertControllerWebViewDidFinishFirstLoad(self)
            delegate?.convertController(self,progress: convertProgressOfTotal * 0.25)
            
            simulateProgress()
            
        } else if hasLink == false {
            print("\(#function) loadCount \(loadCount)")
        }

        
        if !hasLink && urlSession!.hasDownloadLink(webView) {
            print("\(#function) loadCount \(loadCount)")
            hasLink = true
        }
        
    }

    fileprivate func simulateProgress() {
        // simulate progress
        let maxP: Float = 0.88;
        let minP: Float  = 0.30;
        let dur: Float  = 1.00;
        let iter = 8;
        
        for i in 0 ..< iter {
            
            let delay = Float(i+1) * (dur/Float(iter))
            let factor = (Float(i+1)/Float(iter))
            let progressfactor =  minP + ((maxP - minP) * factor )
            let pvalue = convertProgressOfTotal * progressfactor;
            
            let delaySeconds = Double(delay)
            let delayTime = DispatchTime.now() + Double(Int64(delaySeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                if !self.progressDidObtainLink {  // ignore
                    self.delegate?.convertController(self, progress: pvalue)
                } else {
                    print("Ignore SIMULATION PROGRESS")
                }
            }
            
            //print("delay \(delay) pvalue \(pvalue) progressfactor \(progressfactor) factor \(factor)")
        }
    }

    
    // MARK: YoutubeMP3DotOrg Delegate
    
    func youtubeMP3DotOrgModifiedHTMLWebDataAvailable(_ controller:JWYoutubeMP3DotOrg) {
        print("\(#function)")
        // In Data
        //<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        if let webData = controller.modifiedHTMLWebData {
            webView!.load(webData as Data, mimeType: "text/html", textEncodingName: "utf-8", baseURL: urlSession!.audioConverterURL as URL)
            
            delegate?.convertControllerDidInitiateWebView(self)
            delegate?.convertController(self,progress: convertProgressOfTotal * 0.15)
            
        } else {
            // no modifiedHTMLWebData
            delegate?.convertControllerDidNotInitiateWebView(self)
        }
    }
    
    func youtubeMP3DotOrgFailedToProcessWebData(_ controller:JWYoutubeMP3DotOrg) {
        
        delegate?.convertControllerDidNotInitiateWebView(self)
    }

    
    func youtubeMP3DotOrg(_ controller:JWYoutubeMP3DotOrg, didObtainLink linkString:String) {

        progressDidObtainLink = true

        // Notify the delegate and begin the download
        
        delegate?.convertController(self, didObtainLink: linkString)
        delegate?.convertController(self,progress: convertProgressOfTotal)

        if let downloadURL = URL(string: linkString) {
            downloadTask(downloadURL, dbKey: controller.dbkey!)
        } else {
            print("\(#function) Error with linkString \(linkString)")
        }
    }
   
    
    // MARK: Download
    
    fileprivate func downloadTask(_ linkURL:URL, dbKey dbkey:String) {
        
        if state == .cancelled {
            return
        }
        
        state = .downloading

        downloadController = JWFileDownloader()
        
        downloadController!.dowloadFileWithURL(linkURL, progress: { (progress) in

            let totalProgress: Float = self.convertProgressOfTotal + (self.downloadProgressOfTotal * progress);

            self.delegate?.convertController(self,progress: totalProgress)
            
            })
        { (url, dbkeyDownload) in
            
            // Completion, didDownload
            
            self.didDownloadFile(url, dbKey:dbkey)
            
        } // end trailing completion
        
    }
    
    fileprivate func didDownloadFile(_ fileURL:URL, dbKey dbkey:String) {
        
        // print("\(#function) \(dbkey)")
        
        if state == .cancelled {
            return
        }

        if let newFileUrl = fileURLForCacheItem(dbkey) {

            // may or may not exists
            // if it exists, remove it

            var isDir: ObjCBool = false
            if FileManager.default.fileExists(atPath: (newFileUrl.path), isDirectory: &isDir) {
                do {
                    try FileManager.default.removeItem(at: newFileUrl)
                    //print("FILE REMOVED ")
                } catch {
                    print("DID NOT REMOVE FILE Error: \(error)")
                }
            }
            
            // move from downloaded space to Documents
            
            do {
                try FileManager.default.moveItem(at: fileURL, to: newFileUrl)
                
                print("FILE MOVED, SAVED \(newFileUrl.lastPathComponent)")
                // and notify delegate
                self.delegate?.convertController(self, didSaveFileDataForDbKey: dbkey)

            } catch {
                print("DID NOT MOVE FILE Error: \(error)")
            }
        }
        
    }
    
    fileprivate func fileURLForCacheItem(_ dbkey:String?) -> URL? {
        
        var result: URL?
        var fname = "mp3file"
        
        if let dbCacheKey = dbkey {
            fname = "mp3file_\(dbCacheKey)"
        }
        
        result = self.documentsDirectory.appendingPathComponent(fname + ".mp3")
        
        print("\(#function) FileName: \(result!.lastPathComponent)")
        return result
    }
    
    

    // MARK: newSession
    
    fileprivate func newSessionWithLinkURLString(_ linkURLStr:String, forDbKey dbkey:String) {
        
        if let linkURL =  URL(string: linkURLStr) {
            prepareToBeginNewSessionWithLinkURL(linkURL, forDbKey:dbkey)
            startSession()
        }
    }
    
    func newSessionWithLinkURLString(_ linkURLStr:String?, videoTitle:String?) {
        
        print("\(#function) \(linkURLStr) \(videoTitle)")
        
        progressDidObtainLink = false

        if let linkStr = linkURLStr {
            
            let doesConvert = true
//            let doesDownload = false
//            let doesFile = false
            
            //var downLoadLinkURL: NSURL?
            
            let dbkey = cacheItemForLinkStr(linkURLStr, videoTitle: videoTitle)
            
            if let dbCacheKey = dbkey {
                self.currentCacheItem = dbCacheKey
            }
            
            // Decide to GET YouTube DATA for video
            // and init controllers depending on next process
            // convert file download
            
            let retrieveYoutubeData = true
            
            if (doesConvert) {
                
                // Lets setup to convert
                // Notifify the delegate of our intention
                // and prepare for the convert session
                delegate?.convertControllerDoesConvert(self)
                
                if let linkURL = URL(string:linkStr) {
                    prepareToBeginNewSessionWithLinkURL(linkURL, forDbKey:dbkey!)
                }
                
                // A startSession will occur later
                
            } else {
                
            }
            
            // are we gonna use the current file
            // or download from the link recorded
            // or all with the convert process which does all three
            
            if retrieveYoutubeData {

                // use the self youtubeVideoId if has value otherwise derive from url
                //let videoIdOfInterest = youtubeVideoId ?? NSURL(string:linkStr)?.lastPathComponent

                let videoIdOfInterest = URL(string:linkStr)?.lastPathComponent

                if doesConvert {
                    
                    if let videoId = videoIdOfInterest,
                        let dbCacheKey = dbkey
                    {
                        retrieveYoutubeDataDoesConvert(dbCacheKey, videoId:videoId, linkURLString:linkStr, videoTitle:videoTitle)
                        // retrieveYoutubeDataDoesConvert calls startSession()
                    }
                }
                
            } else {
                
                // do not retrieveYoutubeData
                // Since we are not retrieving youtube data we need to proceed normally
                // the same on the completion of retrieving youtube data
                
//                if (doesFile) {
//                    delegate?.convertControllerDoesFile(self, dbKey: dbkey!)
//                } else if (doesDownload) {
//                    
//                }
            }
            
        } else {
            // No Link string
            return
        }
        
    }
    
    fileprivate func cacheItemForLinkStr(_ linkURLStr:String?, videoTitle:String?) -> String? {
        
        var result: String?
        
        if linkURLStr != nil {
            
            var dbkey: String?
            if let dbCacheKey = dbkey {
                print("dbCacheKey \(dbCacheKey)")
                
            } else {
                dbkey = UUID().uuidString;
                result = dbkey
            }
            
        } else {
            print("NO Link String")
        }
        
        return result
    }

    /*
     start session after obtaining video data
     */
    
    fileprivate func retrieveYoutubeDataDoesConvert(_ dbkey:String, videoId:String, linkURLString:String, videoTitle:String?) {
        
        // GET YouTube DATA for video - on completion proceed to convert
        
        let videoDataRetriever = JWYoutubeData()
        
        videoDataRetriever.getVideoData(videoId) {

            (result, videoId) in
            
            if let err = result["statuscode"] {
                print("\(err)")
            } else {
                
                self.imageRetrievalQueue.async {
                    if let thumbnails = result["thumbnailsDict"] as? [String :AnyObject],
                        let imageURL = self.bestImageURLAvailable(thumbnails),
                        let imageData = try? Data(contentsOf: imageURL),
                        let youtubeImage = UIImage(data: imageData) {
                        
                        self.delegate?.convertControllerCoverImageAvailable(self, coverImage: youtubeImage)
                    }
                }
                
                if self.automatic {
                    self.startSession()
                }
                
            }
            
        } // block
    }
    

    fileprivate func bestImageURLAvailable(_ thumbnails:[String :AnyObject]) -> URL? {
        
        var result: URL?

        var hidefSupported = false
        let highResSupported = true

        if UI_USER_INTERFACE_IDIOM() == .pad {
            hidefSupported = true
        }
        
        if hidefSupported {
            
            if let resolution = thumbnails["maxres"] as? [String : AnyObject],
                let imageURLString = resolution["url"] as? String {
                result = URL(string: imageURLString)
            }
            
        } else if highResSupported {
            
            if let resolution = thumbnails["high"] as? [String : AnyObject],
                let imageURLString = resolution["url"] as? String {
                result = URL(string: imageURLString)
            }
        }
        
        if result == nil {
            
            if let resolution = thumbnails["medium"] as? [String : AnyObject],
                let imageURLString = resolution["url"] as? String {
                result = URL(string: imageURLString)
            }
            else if let resolution = thumbnails["standard"] as? [String : AnyObject],
                let imageURLString = resolution["url"] as? String {
                result = URL(string: imageURLString)
            }
            else if let resolution = thumbnails["default"] as? [String : AnyObject],
                let imageURLString = resolution["url"] as? String {
                result = URL(string: imageURLString)
            }
        }
    
        //print(result?.debugDescription)
        return result
    }
    
    
    
    
}

// END CLASS
// ----------------------------------------------

