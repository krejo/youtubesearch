//
//  JWYoutubeSearchModel.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/17/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

// Could be just Foundation, but  UIKit is needed for UIImage and pending UIViewController


// Clients ask for and notify
protocol JWYoutubeSearchResultsDelegate: class  {
    func youtubeSearchResults(_ controller: AnyObject?) -> [YoutubeVideo]?
    func youtubeSearchResultsImageForVideo(_ videoId: String) -> UIImage?
    func youtubeSearchResults(_ controller: AnyObject?, selectedVideo video:YoutubeVideo) -> Void
    func youtubeSearchResults(_ controller: AnyObject?, selectedVideo video:YoutubeVideo, automatic:Bool) -> Void

    func youtubeSearchResults(_ controller: AnyObject?, selectedVideoAtIndex index:Int) -> Void
    func youtubeSearchResults(_ controller: AnyObject?, shouldHighlightItemAtIndex index:Int) -> Bool
    func youtubeSearchResultsSearchReset(_ controller: AnyObject?) -> Void
    func youtubeSearchResults(_ controller: AnyObject?, searchFor searchText:String, newSearch:Bool) -> Void
}


/**
 JWYoutubeSearchResultsDataDelegate protocol implemented by another on
 behalf of JWYoutubeSearchResultsData delegate
 
 */

protocol JWYoutubeSearchResultsDataDelegate: class {
    func searchResultsDataUpdated(_ controller: JWYoutubeSearchResultsData) -> Void
    func searchResultsData(_ controller: JWYoutubeSearchResultsData, updateFor videoID: String) -> Void
    func searchResultsData(_ controller: JWYoutubeSearchResultsData, pendingViewControllerForItemAtIndex index:Int) -> UIViewController?
}

/**
 Search results data Model.  Contains the array of results
 has methods to conduct search
 
 */

class JWYoutubeSearchResultsData: JWYoutubeSearchDelegate, JWPagingConvertDelegate {
    
    var searchResults = [YoutubeVideo]()
    
    weak var delegate: JWYoutubeSearchResultsDataDelegate?
    
    fileprivate var youtubeSearch: JWYoutubeSearch?
    
    init() {
        youtubeSearch = JWYoutubeSearch()
        youtubeSearch?.delegate = self
    }
    
    convenience init(with delegate: JWYoutubeSearchResultsDataDelegate) {
        self.init()
        self.delegate = delegate
    }
    
    
    func getYoutubeDataFor(_ searchText:String, newSearch: Bool) {
        youtubeSearch?.newSearchResults = newSearch
        youtubeSearch?.getYoutubeDataFor(searchText)
    }
    
    func searchYoutubeFor(_ searchText:String, newSearch: Bool) {
        getYoutubeDataFor(searchText, newSearch: newSearch)
    }
    
    func imageForVideo(_ videoId: String) -> UIImage? {
        return youtubeSearch?.imageFor(videoId)
    }
    
    //        searchResults = readValuesFromFile(documentsDirectory.URLByAppendingPathComponent("savelist"))
    //        self.tableView.reloadData()
    

    
    // MARK: YoutubeSearch Delegate
    
    func youtubeSearch(_ youtubeSearch: JWYoutubeSearch, updateWithResults results: [YoutubeVideo]?) -> Void {
        
        print("\(#function)")
        
        if let searchResults = results {
            self.searchResults = searchResults
            delegate?.searchResultsDataUpdated(self)
        }
        
        //        if let saveResults = results {
        //            saveValuesToFile(saveResults, fileURL: documentsDirectory.URLByAppendingPathComponent("savelist"))
        //        }
    }
    
    func youtubeSearch(_ youtubeSearch: JWYoutubeSearch, imageAvailable image: UIImage, videoID: String) -> Void {
        delegate?.searchResultsData(self, updateFor: videoID)
    }
    
    
    // MARK: Paging datasource Delegate
    
    func paging(_ controller:JWPagingConvertDatasource?, itemAtIndex index:Int) -> YoutubeVideo? {
        
        var result: YoutubeVideo?
        if index < pagingNumberOfPages(controller) {
            result  = self.searchResults[index]
        }
        return result
    }
    
    func pagingNumberOfPages(_ controller:JWPagingConvertDatasource?) -> Int {
        var result: Int = 0
        let resultsCount = self.searchResults.count
        result = resultsCount
        return result
    }
    
    func paging(_ controller:JWPagingConvertDatasource?, imageForItemAtIndex index:Int) -> UIImage? {
        
        if let youtubeVideo = paging(nil, itemAtIndex: index) {
            return youtubeSearch?.imageFor(youtubeVideo.videoid!)
        } else {
            return nil
        }
    }
    
    func paging(_ controller:JWPagingConvertDatasource?, pendingViewControllerForItemAtIndex index:Int) -> UIViewController? {
        
        return delegate?.searchResultsData(self, pendingViewControllerForItemAtIndex: index)
    }
    
    
    
    // MARK: - Persistence
    
    func readValuesFromFile<T:PropertyListReadable>(_ fileURL:URL) -> [T] {
        let arrayOfVideos = NSArray(contentsOf: fileURL)
        guard let encodedArray = arrayOfVideos else {return []}
        
        return encodedArray.map{$0 as? NSDictionary}.flatMap{T(propertyListRepresentation:$0)}
    }
    
    func saveValuesToFile<T:PropertyListReadable>(_ newValues:[T], fileURL:URL) {
        let encodedValues = newValues.map{$0.propertyListRepresentation()}
        (encodedValues as NSArray).write(to: fileURL, atomically: true)
    }
    
    func extractValuesFromPropertyListArray<T:PropertyListReadable>(_ propertyListArray:[AnyObject]?) -> [T] {
        guard let encodedArray = propertyListArray else {return []}
        return encodedArray.map{$0 as? NSDictionary}.flatMap{T(propertyListRepresentation:$0)}
    }
    
    func saveValuesToDefaults<T:PropertyListReadable>(_ newValues:[T], key:String) {
        let encodedValues = newValues.map{$0.propertyListRepresentation()}
        UserDefaults.standard.set(encodedValues, forKey:key)
    }

    
}

