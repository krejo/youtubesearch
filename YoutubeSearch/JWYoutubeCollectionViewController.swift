//
//  JWYoutubeCollectionViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/2/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


private let reuseIdentifier = "JWCollectionViewCell"


class JWYoutubeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override var isSelected: Bool {
        willSet {
            if newValue {
                self.layer.borderWidth = 3.2
                self.layer.borderColor = UIColor.white.withAlphaComponent(0.70).cgColor;
                self.layer.cornerRadius = 12.0

            } else {
                self.layer.borderWidth = 0.5
                self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.70).cgColor;
                self.layer.cornerRadius = 6.0
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 6.0
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.70).cgColor;
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}


/**
 Present search results in a layout flow format
 
 */

class JWYoutubeCollectionViewController: UICollectionViewController, ContentPopOverDelegate {

    weak var delegate: JWYoutubeSearchResultsDelegate?

    fileprivate var searchResults: [YoutubeVideo]?
    fileprivate var customPresentationController: JWMiddlePresentationController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Register cell classes
        //self.collectionView!.registerClass(JWYoutubeCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func activated() {
        searchResults = delegate!.youtubeSearchResults(self)
        self.collectionView?.reloadData()
    }
    
    
    func focusOnIndex(_ index:Int, animated:Bool) {
        let indexPath = IndexPath(item: index, section: 0)
        self.collectionView?.scrollToItem(at: indexPath, at: .left, animated: animated)
    }

    func topItemIndex() -> Int {
        var result = 0
        if let visible = self.collectionView?.indexPathsForVisibleItems,
            let topIndex = visible.first?.item
        {
            result = topIndex
        }
        
        return result
    }

    
    override func collectionView(_ collectionView: UICollectionView,
                                   willDisplay cell: UICollectionViewCell,
                                                   forItemAt indexPath: IndexPath)
    
    {
        let youtubeCell = cell as! JWYoutubeCollectionViewCell
       
        if indexPath.row < self.searchResults?.count {
            let youtubeVideo : YoutubeVideo = (self.searchResults?[indexPath.row])!
            youtubeCell.titleLabel.text = youtubeVideo.title
            //            cell.detailTextLabel!.text = youtubeVideo.description
            youtubeCell.imageView.image = delegate!.youtubeSearchResultsImageForVideo(youtubeVideo.videoid!)
        } else {
            youtubeCell.titleLabel!.text = "Load More ...";
            //            cell.detailTextLabel!.text = "if you dare";
            youtubeCell.imageView!.image = nil;
        }

    }
    
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var result: Int = 0
        
        if let resultsCount = self.searchResults?.count {
            result = resultsCount + 1
        }
        
        return result

    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! JWYoutubeCollectionViewCell
            
        // Configure the cell
        
        if let highlight = delegate?.youtubeSearchResults(self, shouldHighlightItemAtIndex:indexPath.row) {
        
            if highlight {
                print("found pending at index \(indexPath.row)")
                let backView = UIView()
                backView.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
                cell.backgroundView = backView
            } else {
                cell.backgroundView = nil
            }
            
        } else {
            print("pending not found at index \(index)")
            cell.backgroundView = nil
        }
        
    
        return cell
    }


    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let youtubeVideo : YoutubeVideo = (self.searchResults?[indexPath.row])!

        // The presentation of MP3 viewcontroller doesnt happen until
        // selectedVideo:youtubeVideo is called
        // yet we will send the selectedVideoAtIndex message
        
        delegate!.youtubeSearchResults(self, selectedVideoAtIndex:indexPath.row)
        
        demoView(youtubeVideo)
        
//        delegate!.youtubeSearchResults(self, selectedVideo:youtubeVideo)
        
    }

    
    override func willTransition(to newCollection: UITraitCollection,
                                           with coordinator: UIViewControllerTransitionCoordinator)

    {
        //print("\(#function)")

        super.willTransition(to: newCollection, with: coordinator)
        
        guard let flowLayout = collectionView!.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }

        if newCollection.verticalSizeClass == .compact {
            //let height = CGRectGetWidth(collectionView!.frame)
            flowLayout.itemSize = CGSize(width: 220, height: 184)
        } else {
        
            flowLayout.itemSize = CGSize(width: 168, height: 142)
        }
        
        //flowLayout.invalidateLayout()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
//        guard let flowLayout = collectionView!.collectionViewLayout as? UICollectionViewFlowLayout else {
//            return
//        }

        coordinator.animate(alongsideTransition: { (context) in
            
            //flowLayout.invalidateLayout()
            
            }) { (context) in
                self.collectionView?.performBatchUpdates({
                    self.collectionView?.setCollectionViewLayout((self.collectionView?.collectionViewLayout)!, animated: true)
                    }, completion: nil)
        }
    }
    
    
    // MARK: ContentPopOverDelegate
    
    fileprivate func contentPopOver(_ controller:ContentPopOverViewController?, convert video: YoutubeVideo) {

        self.dismiss(animated: false) {
            self.delegate!.youtubeSearchResults(self, selectedVideo:video, automatic: true)
        }
    }
    
}


// MARK: - Demo

extension JWYoutubeCollectionViewController {

    func demoView(_ video: YoutubeVideo?) {
        
        let demoViewController = ContentPopOverViewController()
        demoViewController.youtubeVideo = video
        demoViewController.edgesForExtendedLayout = UIRectEdge()
        demoViewController.delegate = self
        
        // .Custom set at init
        self.customPresentationController =
            JWMiddlePresentationController(presentedViewController:demoViewController, presenting: self)
        
        demoViewController.transitioningDelegate = customPresentationController

        present(demoViewController, animated:true, completion:nil)
    }

}

//        let demoViewController = UIViewController()
//        demoViewController.view.backgroundColor = UIColor.blueColor().colorWithAlphaComponent(0.5)

// Using a local may lose on scope
// NS_VALID_UNTIL_END_OF_SCOPE;
//        let myPresentationController = JWPresentationController(presentedViewController:demoViewController,
//                                                                presentingViewController: self)
//        demoViewController.transitioningDelegate = myPresentationController




// MARK: - ContentPopOver

/**
 A Content viewcontroller
 
 */

private protocol ContentPopOverDelegate: class {
    func contentPopOver(_ controller:ContentPopOverViewController?, convert video: YoutubeVideo)
}

private class ContentPopOverViewController: UIViewController, UIAdaptivePresentationControllerDelegate {

    weak var delegate: ContentPopOverDelegate?
    
    var youtubeVideo : YoutubeVideo?
    
    var foregroundContentView : UIVisualEffectView?
    var backgroundView : UIVisualEffectView?

    // For an adaptive presentation, the presentation controller's delegate
    // must be configured prior to invoking
    // -presentViewController:animated:completion:.  This ensures the
    // presentation is able to properly adapt if the initial presentation
    // environment is compact.

    override var transitioningDelegate: UIViewControllerTransitioningDelegate? {
        didSet{
            self.presentationController!.delegate = self
        }
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.clear
        self.backgroundView?.contentView.backgroundColor = UIColor.blue.withAlphaComponent(0.25)
        let dismissButton = UIBarButtonItem(title:"Dismiss", style:.plain, target:self, action:#selector(dismissButtonAction))
        self.navigationItem.leftBarButtonItem = dismissButton
        
        configureView()
    }
    
//    override func loadView() {
//        let blur = UIBlurEffect(style: .Light)
////        let vibrancy = UIVibrancyEffect(forBlurEffect: blur)
////        let v = UIVisualEffectView(effect: vibrancy)
//        let v = UIVisualEffectView(effect: blur)
//        self.view = v
//    }
    
    @IBAction @objc func dismissButtonAction(_ sender:UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UIAdaptivePresentationControllerDelegate
    
    @objc func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        // An adaptive presentation may only fallback to
        // UIModalPresentationFullScreen or UIModalPresentationOverFullScreen
        // in the horizontally compact environment.  Other presentation styles
        // are interpreted as UIModalPresentationNone - no adaptation occurs.
        //
        // .Fullscreen has the effect of standard presentation
        // .OverFullScreen keeps the effect offerd by the animation transitioning
        return .overFullScreen
    }
    
    @objc func presentationController(_ controller:UIPresentationController, viewControllerForAdaptivePresentationStyle style:UIModalPresentationStyle) -> UIViewController? {

        let presentController = UINavigationController(rootViewController: controller.presentedViewController)
        presentController.view.backgroundColor = UIColor.clear
        
        return presentController
    }
    
    
    var titleLabel : UILabel?
    var detailLabel : UILabel?
    var convertButton : UIButton?
    var playButton : UIButton?
    
    func configureView() {
        
        let blur = UIBlurEffect(style: .dark)
        let vibrancy = UIVibrancyEffect(blurEffect: blur)
        
        foregroundContentView = UIVisualEffectView(effect: vibrancy)
        backgroundView = UIVisualEffectView(effect: blur)

        backgroundView?.translatesAutoresizingMaskIntoConstraints = false
        foregroundContentView?.translatesAutoresizingMaskIntoConstraints = false

        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false

        titleLabel!.text = "title"
        titleLabel!.text = youtubeVideo?.title
        titleLabel!.textColor = UIColor.white
        titleLabel!.numberOfLines = 3
        titleLabel!.shadowColor = UIColor.darkGray
        titleLabel!.shadowOffset = CGSize(width: -0.5, height: -0.5)
        titleLabel!.textAlignment = .center

        if traitCollection.horizontalSizeClass == .regular {
            titleLabel?.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        }
        
        detailLabel = UILabel()
        detailLabel!.translatesAutoresizingMaskIntoConstraints = false

        detailLabel!.text = "Detail"
        detailLabel!.text = youtubeVideo?.description
        detailLabel!.textColor = UIColor(white: 0.88, alpha: 1.0)
        detailLabel!.numberOfLines = 5
        
        if traitCollection.horizontalSizeClass == .regular {
            detailLabel?.font = UIFont.systemFont(ofSize: 24)
        } else {
            detailLabel?.font = UIFont.systemFont(ofSize: 17)
        }

        //convertButton = UIButton(type: .System)
        convertButton = UIButton(type: .custom)
        convertButton!.translatesAutoresizingMaskIntoConstraints = false
        convertButton!.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        convertButton!.setTitle("Convert", for: UIControlState())
        
        self.view.addSubview(backgroundView!)
//        self.view.addSubview(foregroundContentView!)

        if let contentView = backgroundView?.contentView {
            
            //contentView.addSubview(label!)
            contentView.addSubview(titleLabel!)
            contentView.addSubview(detailLabel!)
            contentView.addSubview(convertButton!)
            
            // Layout
            
            let views: [String: AnyObject] = [
                "backgroundView" :  backgroundView!,
                "foregroundContentView" : foregroundContentView!,
                "titleLabel" : titleLabel!,
                "detailLabel" : detailLabel!,
                "convertButton" : convertButton!
            ]

            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|",
                options: [], metrics: nil, views: views))
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|",
                options: [], metrics: nil, views: views))
            
//            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[foregroundContentView]|",
//                options: NSLayoutFormatOptions(rawValue: 0),
//                metrics: nil, views: views))
//            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[foregroundContentView]|",
//                options: NSLayoutFormatOptions(rawValue: 0),
//                metrics: nil, views: views))

            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(50)-[titleLabel]",
                options: [], metrics: nil, views: views))
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[titleLabel]-(20)-|",
                options: [], metrics: nil, views: views))

            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[titleLabel]-(10)-[detailLabel]",
                options: [], metrics: nil, views: views))
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[detailLabel]-(20)-|",
                options: [], metrics: nil, views: views))


            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[detailLabel]-20@250-[convertButton]-(80)-|",
                options: [.alignAllCenterX],
                metrics: nil, views: views))

            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[convertButton(100)]",
                options: [],
                metrics: nil, views: views))

            
            self.view.layoutIfNeeded()
            
            convertButton!.grayNormal(2.0)  // Gradient Button
        }
        
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        print(#function)
        if let video = youtubeVideo {
            delegate?.contentPopOver(self, convert: video)
        }
    }
    

}



/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
 return true
 }
 // Uncomment this method to specify if the specified item should be selected
 override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
 return true
 }
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
 return false
 }
 
 override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
 return false
 }
 
 override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
 
 }
 */

