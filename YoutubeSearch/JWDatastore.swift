//
//  JWDatastore.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation


// NOTE None of the data structs below have been put to use yet

// Audio is obtained throu various sources
// Audio files used in the system have a unique identifier which
// additional info about that audio can be be obtained
// Such as title and artist
// Date it entered the system
// Source
//   - was it from Youtube
//   - was Brought in by the User through airDrop/share or public documents in iTunes
//

// Stored Links

struct Link {
    var dbkey: String?
    var linkstr: String?
}

extension Link: PropertyListReadable {
    func propertyListRepresentation() -> NSDictionary {
        let representation:[String:AnyObject] = ["dbkey":self.dbkey! as AnyObject, "linkstr":self.linkstr! as AnyObject]
        return representation as NSDictionary
    }
    
    init?(propertyListRepresentation:NSDictionary?) {
        guard let values = propertyListRepresentation else {return nil}
        if let dbkey = values["dbkey"] as? String,
            let linkstr = values["linkstr"] as? String {
            
            self.dbkey = dbkey
            self.linkstr = linkstr
        } else {
            return nil
        }
    }
}



// Info about youtube record

struct YoutubeInfo {
    var youtubeVideo : YoutubeVideo
    var artist: String?
    var thumbURLStringDefault: String?
    var thumbURLStringSmall: String?
    var thumbURLStringMedium: String?
    var thumbURLStringLarge: String?
    var thumbURLStringHD: String?
}


enum SourceOfAudio {
    case youtube
    case userObtained
}


// Rememberd MP3 Items

struct MP3DataRecord {
    var dbkey: String?
    var downloadlinkstr: String?
    var creationdate: Date
    var source: SourceOfAudio
}

struct DBRecords {
    var items: [MP3DataRecord]
    
}

struct LinkRecords {
    var items: [Link]
    
}


// All Datastore

struct DataStore {
    var mp3Records: DBRecords
    var linkRecords: LinkRecords
}

