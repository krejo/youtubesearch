//
//  JKFileDownloadController.swift
//  FileManager
//
//  Created by JOSEPH KERR on 3/6/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class JWFileDownloader: NSObject, URLSessionDelegate {
    
    typealias completionMethod = (_ url:URL, _ dbKey:String) -> Void

    // MARK: Properties

    fileprivate var completionHandler: completionMethod?
    fileprivate var progressHandler: ((_ progress:Float) -> Void)?
    fileprivate var downloadTask: URLSessionDownloadTask?
    fileprivate var session: Foundation.URLSession?

    func dowloadFileWithURL(_ url: URL) {

        print("download file...")

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        session = Foundation.URLSession(configuration: URLSessionConfiguration.default,
            delegate: self, delegateQueue: nil)
        
        downloadTask = session?.downloadTask(with: url)
        downloadTask?.resume()
    }

    func dowloadFileWithURL(_ url: URL, completion: @escaping completionMethod) {
        completionHandler = completion
        dowloadFileWithURL(url)
    }
    
    func dowloadFileWithURL(_ url: URL, progress:@escaping (_ progress:Float)->Void, completion: @escaping completionMethod) {
        completionHandler = completion
        progressHandler = progress
        dowloadFileWithURL(url)
    }

    func cancel() {
        downloadTask?.cancel()
        session?.invalidateAndCancel()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    // MARK: session delegate
    
    func URLSession(_ session: Foundation.URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingToURL location: URL) {
            
            //print("finished \n\(location)")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if let completion = completionHandler {
                completion(location,"key")
            }
        
            session.finishTasksAndInvalidate()
    }
    
    func URLSession(_ session: Foundation.URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64)
    {
        var percentComplete = 0.0
        
        if totalBytesExpectedToWrite > 0 {
            percentComplete = Double(totalBytesWritten)/Double(totalBytesExpectedToWrite);
        }
        
        if let progress = progressHandler {
            progress(Float(percentComplete))
        }
    }
    
}


