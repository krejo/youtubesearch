//
//  JWYoutubeSearch.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/29/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

/**
 JWYoutubeSearchDelegate protocol
 
 JWYoutubeSearch will notifiy o fthe following events
 */

protocol JWYoutubeSearchDelegate: class {
    func youtubeSearch(_ youtubeSearch: JWYoutubeSearch, updateWithResults results: [YoutubeVideo]?) -> Void
    func youtubeSearch(_ youtubeSearch: JWYoutubeSearch, imageAvailable image: UIImage, videoID: String) -> Void
}

/**
 Conducts a youtube search and creates instances of `YoutubeVideo` types
 
 */

class JWYoutubeSearch: NSObject, NSCacheDelegate {

    fileprivate var searchResults = [YoutubeVideo]()
    fileprivate var youtubeSearchData = JWYoutubeSearchData()
    fileprivate var imageRetrievalQueue: DispatchQueue!
    fileprivate var imagePlaceholder = UIImage(named: "imageplaceholder")!
    fileprivate var imageCache: NSCache<NSString,UIImage> = NSCache()
    fileprivate var mostRecentSearchString: String?
    
    weak var delegate: JWYoutubeSearchDelegate?

    var newSearchResults = true
    
    override init() {
        imageRetrievalQueue = DispatchQueue(label: "com.joker.imageProcessing",attributes: DispatchQueue.Attributes.concurrent)
        super.init()
        imageCache.delegate = self
    }
    
    
    /// Conducts a youtube video search for a given `searchText`.
    ///
    /// Usage:
    ///
    ///     getYoutubeDataFor("acdc")) // search for acdc!
    ///
    /// - parameters:
    ///   - String: searchText, text to search for
    /// - returns: Void
    
    func getYoutubeDataFor(_ searchText:String) {

        print("\(#function) \(searchText) newSearch \(newSearchResults)")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        mostRecentSearchString = searchText
        if newSearchResults {
            searchResults.removeAll()
        }
        
        youtubeSearchData.getSearchResultsFor(searchText, newSearch: newSearchResults, resultsSoFar: 0) {
            
            (results, searchString) in

            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if self.mostRecentSearchString! == searchString {
                
                // Create Video search results and send to delegate
                
                for result in results {
                    let video = YoutubeVideo(title: result["title"] as? String,
                                             description: result["description"] as? String,
                                             videoid: result["videoid"] as? String,
                                             thumbURLString: result["thumbnail"] as! String)
                    
                    self.searchResults += [video]
                }
                
                self.delegate?.youtubeSearch(self, updateWithResults: self.searchResults)
                
                // Get the thumbnail images
                
                for result in results {
                    let videoID = result["videoid"] as? String
                    let thumbURL = result["thumbnail"] as! String
                    self.imageFor(videoID!, imageURLString: thumbURL)
                }
                
            } else {
                print("outadated response search text \(searchText) current \(self.mostRecentSearchString!)")
            }
            
        } // trailing closure getSearchResultsFor

    }
    
    
    /// Returns a thumbnail image for video id.
    ///
    /// First looks in loacal cache and if it doesnt exist there then returns a placeholder image
    ///
    /// Usage:
    ///
    ///     imageFor("zxcffgj"))
    ///
    /// - parameters:
    ///   - String: videoID
    /// - returns: UIImage?

    func imageFor(_ videoID:String) -> UIImage? {
        
        var result: UIImage?
        if let cachedImage = imageCache.object(forKey: videoID as NSString) {
            result = cachedImage
        } else {
            result = imagePlaceholder
        }
        return result
    }

    
    /// Retrieves a thumbnail image for video id and imageURL String
    ///
    /// The request to obtain data is asynchronous, submitted on `imageRetrievalQueue`
    ///
    /// Usage:
    ///
    ///     imageFor("zxcffgj","http:/gogogle/thumbnailvideo.jpg")
    ///
    ///
    /// - parameters:
    ///   - String: videoID
    ///   - String: imageURLString
    /// - returns: Void

    func imageFor(_ videoID:String, imageURLString:String) -> Void {
        
        self.imageRetrievalQueue.async {
            if let imageURL = URL(string: imageURLString),
                let imageData = try? Data(contentsOf: imageURL),
                let youtubeImage = UIImage(data: imageData) {

                self.imageCache.setObject(youtubeImage, forKey: videoID as NSString)
                self.delegate?.youtubeSearch(self, imageAvailable:youtubeImage, videoID: videoID)
            }
        }
    }
    
    func cache(_ cache: NSCache<AnyObject, AnyObject>, willEvictObject obj: Any) {
        print("\(#function) \(obj)")
    }
    
}






