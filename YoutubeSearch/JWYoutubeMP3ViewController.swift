//
//  JWYoutubeMP3ViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/30/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class JWYoutubeMP3ViewController: UIViewController, JWYoutubeConvertDelegate, JWPresentationControllerDelegate {

    // MARK: Outlets

    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var effectsView: UIVisualEffectView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var convertButton: UIButton!
    @IBOutlet weak var watchButton: UIButton!

    // MARK: Private

    fileprivate var showsYoutubeMP3 = true  // shows the youtube MP3 Webview
    fileprivate var proceedForwardOnSuccess = true
    fileprivate var proceedForwardToPlay = true
    fileprivate var useSwipeDownToReConvert = false
    fileprivate var useLongPressToPaste = false
    fileprivate var firstTime = true
    fileprivate var cancelled = false

    fileprivate var currentMP3FileURL: URL?
    fileprivate var mp3ConvertController: JWYoutubeConvertController?
    fileprivate var youtubeVideoId: String?
    fileprivate var youtubeVideoTitle: String?

    fileprivate var customPresentationController: JWPresentationController?

    fileprivate var avPlayer: AVPlayer?
    
    // MARK: Public
    
    var automatic = true
    var tapToJam = false
    var youTubeLinkURL: URL?
    var startingCoverImage: UIImage?
    var pageIndex: Int? // for paging
    var convertUserActivated = false  // convert activated by user
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

    func setUrlSession(_ youtubeLinkString: String, videoId:String, videoTitle:String) {
        youtubeVideoId = videoId;
        youtubeVideoTitle = videoTitle;
        youTubeLinkURL = URL(string: youtubeLinkString)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        activity.stopAnimating()
        effectsView.isHidden = false
        webView.isHidden = true
        imageView.isHidden = true
        progressView.isHidden = true

        progressView.transform = CATransform3DGetAffineTransform(CATransform3DMakeScale(1.0, 8.0, 1.0));
        progressView.progress = 0.0
//        imageView.layer.borderWidth = 3.2
//        imageView.layer.borderColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.70).CGColor;
//        imageView.layer.cornerRadius = 12.0
//        imageView.layer.masksToBounds = true
//        imageView.backgroundColor = UIColor.blackColor()
        if let coverImage = startingCoverImage {
            imageView.image = coverImage
            imageView.isHidden = false
        }

        if (self.automatic) {
            convertButton.isHidden = true
            watchButton.isHidden = true
        } else {
            proceedForwardOnSuccess = false
        }

        
        var titleString = ""
        if (automatic) {
            titleString = "Converting to MP3"
            if let statusTitle = youtubeVideoTitle  {
                titleString += "\n\(statusTitle)"
            }
            
        } else {
            if let statusTitle = youtubeVideoTitle  {
                titleString = statusTitle
            }
        }
        
        self.statusLabel.text = titleString;
        
        firstTime = true
        
        mp3ConvertController = JWYoutubeConvertController(delegate: self, webView: webView)
        
        convertButton.grayNormal(3.0)
        watchButton.grayNormal(3.0)

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if firstTime {
            if let coverImage = startingCoverImage {
                imageView.image = coverImage
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //print("\(#function) firstTime \(firstTime)")
        
        if firstTime {
            firstTimeNewSession()
            firstTime = false
            //print("\(#function)")
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Do not cancel in a Paging implementation
        var cancelling = true
        if let _ = pageIndex {
            cancelling = false
        }
        
        if cancelling {
            
            if self.isMovingFromParentViewController {
                activity.stopAnimating()
                cancelled = true
                print("isMovingFromParentViewController")
                mp3ConvertController?.delegate = nil
                mp3ConvertController?.cancel()
            }
        }
    }
    
    
    // Helper
    
    func firstTimeNewSession(){
        
        if let linkURL = self.youTubeLinkURL {
            
            // newSessionWithLinkURLString(linkURL.absoluteString, videoTitle: nil)
            
            newSessionWithLinkURLString(linkURL.absoluteString, videoTitle: youtubeVideoTitle)

            
        } else {
            
            // not given link try to obtain active(current) one
            
            if let currentDBKey = UserDefaults.standard.value(forKey: "currentItem") {
                
                activity.startAnimating()
                let currentCacheItem = currentDBKey as? String
                if let mp3FileURL = fileURLForCacheItem(currentCacheItem) {
                    self.currentMP3FileURL = mp3FileURL
                    
                    // TODO: API current item
                    // [JWCurrentWorkItem sharedInstance].currentAudioFileURL = _currentMP3FileURL;
                }
            }
        }
        
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "JWMP3ToYoutubeWebViewSegue" {
            
            if let navigationController = segue.destination as? UINavigationController {
                if let youtubeWebViewController = navigationController.viewControllers.first as? JWYoutubeWebViewController {
                    
                    //if let youtubeWebViewController = segue.destinationViewController as? JWYoutubeWebViewController {
                    
                    youtubeWebViewController.youTubeLinkURL = youTubeLinkURL
                }
            }
        }
    }
    
    
    // MARK: -

    fileprivate func newSessionWithLinkURLString(_ linkURLStr:String?, videoTitle:String?) {
        
        //print("\(#function) \(linkURLStr)")
        
        if let linkStr = linkURLStr {
            
            cancelled = false
            // Init UI for begin process
            self.webView.isHidden = true
            self.effectsView.isHidden = false
            if automatic {
                activity.startAnimating()
            }

            mp3ConvertController?.automatic = automatic
            mp3ConvertController?.newSessionWithLinkURLString(linkStr, videoTitle: videoTitle)
            
        } else {
            effectsBackgroundError()
        }
    }

    fileprivate func finalStatusMessage(_ dbkey:String?) {
    
        var statusTitle = ""
        
        if let dbCacheKey = dbkey {
            print("dbCacheKey \(dbCacheKey)")
//            id mp3DataRecord = _mp3FilesInfo[dbkey];
//            if (mp3DataRecord)
//            statusTitle = mp3DataRecord[JWDbKeyYouTubeData][JWDbKeyYouTubeTitle];
            
            statusTitle = "mp3DataRecord title"
        }
    
        var titleString: String

        statusTitle = youtubeVideoTitle ?? ""

        if tapToJam {
            titleString = "Tap To JAM With\n\(statusTitle)"
        } else {
            titleString = "Converted to MP3\n\(statusTitle)"
        }
    
        DispatchQueue.main.async{
            self.statusLabel.text = titleString;
        }
    }

    
    fileprivate func proceedForwardAction(_ dbkey:String?) {
    
        if cancelled {
            return
        }
        
        if automatic || convertUserActivated {
            
            if proceedForwardToPlay {
                
                if currentMP3FileURL != nil {
                    playFile()
                } else {
                    effectsBackgroundError()
                }
                
            } else {

                if tapToJam {
                    performSegue(withIdentifier: "JWMP3DownloadToClipEngineSegue", sender: self)
                    
                } else {
                    if currentMP3FileURL != nil {
                        playFile()
                    } else {
                        effectsBackgroundError()
                    }
                }
            }
        }
        
    }
    
    fileprivate func fileURLForCacheItem(_ dbkey:String?) -> URL? {
        var result: URL?
        var fname = "mp3file"
        if let dbCacheKey = dbkey {
            fname = "mp3file_\(dbCacheKey)"
        }
        result = self.documentsDirectory.appendingPathComponent(fname + ".mp3")
        //print("\(#function) FileName: \(result!.lastPathComponent)")
        return result
    }
   

    // MARK: - mp3Converter controler delegate
    
    func convertControllerDoesConvert(_ controller:JWYoutubeConvertController) {
        print("\(#function)")

        // Will begin converting
        self.webviewHeightConstraint.constant = 0 // shows converted
        DispatchQueue.main.async{
            self.view.layoutIfNeeded()
        }
        
        var titleString = ""
        if (automatic) {
            titleString = "Converting to MP3"
            if let statusTitle = youtubeVideoTitle  {
                titleString += "\n\(statusTitle)"
            }

        } else {
            if let statusTitle = youtubeVideoTitle  {
                titleString = statusTitle
            }
        }
        
        DispatchQueue.main.async{
            self.statusLabel.text = titleString;
        }
    }
    
    func convertControllerDoesFile(_ controller:JWYoutubeConvertController, dbKey dbkey:String) {
        print("\(#function)")

        if proceedForwardOnSuccess {
            DispatchQueue.main.async{
                self.statusLabel.text = ""
                self.proceedForwardAction(dbkey)
            }
        } else {
            finalStatusMessage(dbkey)
        }
    }
    
    
    func convertController(_ controller:JWYoutubeConvertController, progress:Float) -> Void {
        
        DispatchQueue.main.async{
            self.progressView.isHidden = false
            self.progressView.setProgress(progress, animated:true)
        }

    }

    
    func convertControllerDidInitiateWebView(_ controller:JWYoutubeConvertController) {
        print("\(#function)")
        
        if !automatic {
            var titleString = "Converting to MP3"
            if let statusTitle = youtubeVideoTitle  {
                titleString += "\n\(statusTitle)"
            }
            
            DispatchQueue.main.async{
                self.statusLabel.text = titleString;
            }
        }
        
        DispatchQueue.main.async{
            self.progressView.isHidden = false
        }

        // just submitted the replaceURL ... and the process has started
        // the end of nitialization
        
        if showsYoutubeMP3 {
            // self.view.sendSubviewToBack(imageView)  // send the artwork to back
            self.view.bringSubview(toFront: webView) // send the artwork to back
            webView.isUserInteractionEnabled = true
            self.webView.isHidden = false
            self.effectsView.isHidden = true
        }
        
        self.activity.startAnimating()
        
        // things are going
        
    }
    
    func convertControllerDidNotInitiateWebView(_ controller:JWYoutubeConvertController) {
        print("\(#function)")
        
        // problem with web load its not going to convert
        DispatchQueue.main.async{
            self.statusLabel.text = "Sorry, There was problem with Converting"
        }
        effectsBackgroundError()
    }
    
    func convertControllerWebViewDidFinishFirstLoad(_ controller:JWYoutubeConvertController) {
        print("\(#function)")
        if showsYoutubeMP3 {
            self.webviewHeightConstraint.constant = 228 // shows converted
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                }, completion: nil)
        }
    }
    
//    func convertController(controller:JWYoutubeConvertController, downloadProgress progress:Float) {
//
////        let totalProgress: Float = self.convertProgressOfTotal + (self.downloadProgressOfTotal * progress);
////        dispatch_async(dispatch_get_main_queue()) {
////            self.progressView.setProgress(totalProgress, animated: true)
////        }
//    }
    
    func convertController(_ controller:JWYoutubeConvertController, didObtainLink linkString:String) {
        print("\(#function) \(linkString)")
        // We are done converting
        
        DispatchQueue.main.async{
            self.stopActivityAnimated()
        }

        // typically NO, dont show youtube MP3 webview
        if showsYoutubeMP3 {
            self.webviewHeightConstraint.constant = 310; // shows converted
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                }, completion: nil)
            
            let delaySeconds1 = 1.5 + 1.0; // plus animdelay
            let delayTime1 = DispatchTime.now() + Double(Int64(delaySeconds1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime1) {
                self.effectsView.isHidden = false
            }
            
            let delaySeconds2 = 1.5 + 1.0; // plus animdelay
            let delayTime2 = DispatchTime.now() + Double(Int64(delaySeconds2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime2) {
                self.webView.isHidden = true
            }
        }
        
    }
    
    func convertController(_ controller:JWYoutubeConvertController, didSaveFileDataForDbKey dbkey:String) {
        print("\(#function)")
        
        if let fileUrl = fileURLForCacheItem(dbkey) {
            
            self.currentMP3FileURL = fileUrl
            // save for restarts - last item used
            UserDefaults.standard.set(dbkey, forKey: "currentItem")
        }
        
        if showsYoutubeMP3 {
            DispatchQueue.main.async {
                self.webView.isHidden = true
            }
        }

        // Conversion finished
        
        if self.proceedForwardOnSuccess {
            DispatchQueue.main.async {
                self.progressView.isHidden = true
                self.proceedForwardAction(dbkey)
            }
            
        } else {
            self.effectsBackgroundSuccess()
            self.finalStatusMessage(dbkey)
            
            let delaySeconds = 0.25
            let delayTime = DispatchTime.now() + Double(Int64(delaySeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                    self.progressView.alpha = 0.1
                }) { (fini:Bool) in
                    self.progressView.isHidden = true
                }
            }
        }
    }
    
    func convertControllerCoverImageAvailable(_ controller:JWYoutubeConvertController, coverImage image:UIImage) -> Void {
        DispatchQueue.main.async {
            self.imageView.image = image
            self.imageView.isHidden = false
        }
    }

    
    // MARK: -
    
    fileprivate func stopActivityAnimated() {
        
        let popScaleTransform = CATransform3DGetAffineTransform(CATransform3DMakeScale(1.15, 1.150, 1.0));
        let scaleTransform = CATransform3DGetAffineTransform(CATransform3DMakeScale(0.25, 0.250, 1.0));
        DispatchQueue.main.async{
            UIView.animate(withDuration: 0.20, delay: 0.0, options: UIViewAnimationOptions(), animations: {
                self.activity.transform = popScaleTransform
            }) { (fini:Bool) in
                
                if fini {
                    UIView.animate(withDuration: 0.65, delay: 0.0, options: .curveEaseOut, animations: {
                        self.activity.transform = scaleTransform
                        self.activity.alpha = 0.0
                    }) { (fini:Bool) in
                        self.activity.stopAnimating()
                        self.activity.transform = CATransform3DGetAffineTransform(CATransform3DIdentity)
                        self.activity.alpha = 1.0
                    }
                } else {
                    self.activity.stopAnimating()
                    self.activity.transform = CATransform3DGetAffineTransform(CATransform3DIdentity)
                    self.activity.alpha = 1.0
                }
            }
        }
    }
    
    // MARK: - Button Actions
    
    @IBAction func convertRequested(_ sender: UIButton) {
        //mp3ConvertController?.newSessionWithLinkURLString(linkStr, videoTitle: videoTitle)
        convertUserActivated = true
        self.convertButton.isEnabled = false
        mp3ConvertController?.startSession()
    }
 
    
    @IBAction func watchRequested(_ sender: AnyObject) {
        
        
    }

    
    // MARK: Effects views
    
    fileprivate func effectsBackgroundError() {
        print("\(#function)")
        
        let cb = self.view.backgroundColor
        DispatchQueue.main.async{
            self.view.backgroundColor = UIColor.red.withAlphaComponent(0.4)
        }
        
        let delaySeconds1 = 1.5
        let delayTime1 = DispatchTime.now() + Double(Int64(delaySeconds1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime1) {
            self.view.backgroundColor = cb
        }
        
    }
    
    fileprivate func effectsBackgroundSuccess() {
        print("\(#function)")
        
        let cb = self.view.backgroundColor
        
        DispatchQueue.main.async{
            self.view.backgroundColor = UIColor.blue.withAlphaComponent(0.4)
        }
        
        var blink = true
        if let _ = pageIndex {
            blink = false
        }
        
        if blink {
            let delaySeconds1 = 0.75
            let delayTime1 = DispatchTime.now() + Double(Int64(delaySeconds1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime1) {
                self.view.backgroundColor = cb
            }
        }
    }
    

    // MARK: Gestures

    @IBAction func didSwipeDown(_ sender: AnyObject) {
        print("\(#function)")

        if useSwipeDownToReConvert {
            mp3ConvertController!.reconvert()
        }
    }

    @IBAction func didSwipeLeft(_ sender: AnyObject) {
        print("\(#function)")
    }

    @IBAction func longPress(_ sender: AnyObject) {
        print("\(#function)")
        if let gesture = sender as? UIGestureRecognizer {
            if gesture.state == .began {
                print("\(#function) .Began")

                presentDemoView()
            }
        }
        
    }

    @IBAction func didTap(_ sender: AnyObject!) {
        
        if currentMP3FileURL == nil {
            effectsBackgroundError()
        } else {
            proceedForwardAction(nil)
        }
    }
    
    
    // MARK: player and effects
    
    fileprivate func playFile() {
        print("\(#function)")
        //playFileUsingAVPlayer()
        playFileUsingStandardAVPlayer()
        //playFileUsingAVPlayerContainer()
    }
    
    
}



// MARK: Play file extension

extension JWYoutubeMP3ViewController {
    
    func playFileUsingStandardAVPlayer() {

        print("play \(currentMP3FileURL!.lastPathComponent)")
        
        let myPlayer = AVPlayer(url: self.currentMP3FileURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = myPlayer
        playerViewController.showsPlaybackControls = true
        
        present(playerViewController, animated:false, completion: {
            
            myPlayer.play()
            
            if self.proceedForwardToPlay {
                self.finalStatusMessage(nil)
            }
        })
    }
    
    func playFileUsingAVPlayer() {
        
        print("play \(currentMP3FileURL!.lastPathComponent)")
        
        avPlayer = AVPlayer(url: self.currentMP3FileURL!)
        
        let playerViewController = AVPlayerViewController()
        
        playerViewController.player = avPlayer
        playerViewController.showsPlaybackControls = true
        
        // width doesnt matter
        playerViewController.preferredContentSize = CGSize(width: 0, height: 66) // enough for scrubber and message
        
        // NS_VALID_UNTIL_END_OF_SCOPE;
        // .Custom set at init
        
        self.customPresentationController =
            JWPresentationController(presentedViewController:playerViewController, presenting: self)
        
        customPresentationController?.useRounded = false
        customPresentationController?.offsetFromBottom = 10.0
        customPresentationController?.delegateController = self
        
        playerViewController.transitioningDelegate = customPresentationController
        
        present(playerViewController, animated:true, completion:{
            self.avPlayer!.play()
        })
        
    }
    
    func playFileUsingAVPlayerContainer() {
        
        print("play \(currentMP3FileURL!.lastPathComponent)")

        // self.storyboard is not correct
        //  self.storyboard?.instantiateViewControllerWithIdentifier("AVPlayerContainerView") as? JWPlayerContainerViewController {
        
        if let avPlayerContainer = UIStoryboard(name: "YoutubeMain",
            bundle:nil).instantiateViewController(withIdentifier: "AVPlayerContainerView") as? JWPlayerContainerViewController {
            
            avPlayerContainer.audioFileURL = currentMP3FileURL
            avPlayerContainer.preferredContentSize = CGSize(width: 0, height: 66) // enough for scrubber and message
            
            self.customPresentationController =
                JWPresentationController(presentedViewController:avPlayerContainer, presenting: self)
            
            customPresentationController?.useRounded = false
            //customPresentationController?.offsetFromBottom = 10.0
            //customPresentationController?.delegateController = self
            
            avPlayerContainer.transitioningDelegate = customPresentationController
            
            present(avPlayerContainer, animated:true, completion:{
                avPlayerContainer.play()
            })

        }
    }

    
    func presentationControllerDidDismiss(_ controller: JWPresentationController) {
        print(#function)
        avPlayer?.pause()
        avPlayer = nil
    }

    
  
}


// MARK: Present demo views

extension JWYoutubeMP3ViewController {
    
    func presentDemoView() {
        
        //        let demoViewController = UIViewController()
        //        demoViewController.view.backgroundColor = UIColor.blueColor().colorWithAlphaComponent(0.5)
        //        demoViewController.preferredContentSize = CGSize(width: 0, height: 200)
        
        let demoViewController = ContentPopOverViewController()
        demoViewController.preferredContentSize = CGSize(width: 0, height: 200)
        
        
        // NS_VALID_UNTIL_END_OF_SCOPE;
        // .Custom set at init
        self.customPresentationController =
            JWPresentationController(presentedViewController:demoViewController, presenting: self)
        
        customPresentationController?.useRounded = true
        
        demoViewController.transitioningDelegate = customPresentationController
        
        //        let myPresentationController = JWPresentationController(presentedViewController:demoViewController,
        //                                                                presentingViewController: self)
        //        demoViewController.transitioningDelegate = myPresentationController
        
        present(demoViewController, animated:true, completion:nil)
    }
}



private class ContentPopOverViewController: UIViewController {
    
    override func viewDidLoad() {
        //(self.view as! UIVisualEffectView).contentView.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.15)
        //(self.view as! UIVisualEffectView).contentView.backgroundColor = UIColor.clearColor()
        (self.view as! UIVisualEffectView).contentView.backgroundColor = UIColor.white.withAlphaComponent(0.15)
        //self.view.backgroundColor = UIColor.blueColor()
    }
    
    override func loadView() {
        let blur = UIBlurEffect(style: .light)
        //let vibrancy = UIVibrancyEffect(forBlurEffect: blur)
        //let v = UIVisualEffectView(effect: vibrancy)
        let v = UIVisualEffectView(effect: blur)
        self.view = v
    }
    
}



