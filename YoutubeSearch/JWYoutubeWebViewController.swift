//
//  JWYoutubeWebViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/7/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class JWYoutubeWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!

    var youTubeLinkURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.allowsInlineMediaPlayback = true
        self.webView.loadRequest(URLRequest(url: youTubeLinkURL!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print(#function)
    }
    
    @IBAction func donePressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion:nil)
    }

}
