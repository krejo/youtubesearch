//
//  JWYoutube.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/2/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation

/**
 Video data type
 
 */

struct YoutubeVideo {
    var title: String?
    var description: String?
    var videoid: String?
    var thumbURLString: String
}

extension YoutubeVideo: Equatable { }

func ==(lhs:YoutubeVideo, rhs: YoutubeVideo) -> Bool {
    let equals = lhs.title == rhs.title &&
        lhs.description == rhs.description &&
        lhs.videoid == rhs.videoid
    
    return equals
}

// Persistence helper
// github: iSame7/StructPersistorSwiftPlayground
// MARK: - Property list conversion protocol

protocol PropertyListReadable {
    func propertyListRepresentation() -> NSDictionary
    init?(propertyListRepresentation:NSDictionary?)
}

// MARK: -

extension YoutubeVideo: PropertyListReadable {
    
    func propertyListRepresentation() -> NSDictionary {
        let representation:[String:AnyObject] = ["title":self.title! as AnyObject, "description":self.description! as AnyObject, "videoid":self.videoid! as AnyObject, "thumbURLString":self.thumbURLString as AnyObject]
        return representation as NSDictionary
    }
    
    init?(propertyListRepresentation:NSDictionary?) {
        guard let values = propertyListRepresentation else {return nil}
        if let title = values["title"] as? String,
            let description = values["description"] as? String,
            let videoid = values["videoid"] as? String,
            let thumbURLString = values["thumbURLString"] as? String {
            
            self.title = title
            self.description = description
            self.videoid = videoid
            self.thumbURLString = thumbURLString
            
        } else {
            return nil
        }
    }
}





