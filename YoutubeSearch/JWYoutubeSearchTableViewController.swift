//
//  JWYoutubeSearchTableViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/29/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


/**
  Displays list of youtube videos results from search
 
 */

class JWYoutubeSearchTableViewController: UITableViewController,  UISearchBarDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: Private

    fileprivate var searchResults: [YoutubeVideo]?
    fileprivate var selectedYoutubeVideo: YoutubeVideo?
    fileprivate var moreSearching = false

    // MARK: Public

    weak var delegate: JWYoutubeSearchResultsDelegate?

    var selectedIndex = 0
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    let youtubeLinkWithoutId = "http://youtu.be/"
    var searchTerm : String?  // to be read by outside
    var searchString : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(documentsDirectory)")
        self.clearsSelectionOnViewWillAppear = false;
        
        NotificationCenter.default.addObserver(self, selector: #selector(JWYoutubeSearchTableViewController.searchTermModified(_:)), name: NSNotification.Name(rawValue: "searchTermChanged"), object: self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print(#function)
    }
    
    
    func focusOnIndex(_ index:Int, animated:Bool) {
        let indexPath = IndexPath(row: index, section: 0)
        self.tableView?.scrollToRow(at: indexPath, at: .top, animated: animated)
    }
    
    func topItemIndex() -> Int {
        var result = 0
        if let visible = self.tableView.indexPathsForVisibleRows,
            let topIndex = visible.first?.row {
            result = topIndex
        }
        return result
    }
    
    
    // MARK: Search bar delegate and listener
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchString = searchBar.text
        searchBar.resignFirstResponder()
        if let searchText = searchString {
            delegate?.youtubeSearchResults(self, searchFor: searchText, newSearch:true)
        }

        delegate?.youtubeSearchResultsSearchReset(self)
    }
    
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        
//        let searchTextModifiedNoti = NSNotification(name: "searchTermChanged", object: self, userInfo: ["text":searchText])
//
//        NSNotificationQueue.defaultQueue().enqueueNotification(searchTextModifiedNoti, postingStyle: .PostWhenIdle, coalesceMask: .NoCoalescing, forModes: nil)
//    }

    func searchTermModified(_ noti:Notification) {
        let searchText = noti.userInfo!["text"];
        print("\(#function) \(searchText!)")
        searchString = searchText as? String
        delegate?.youtubeSearchResults(self, searchFor: searchString!, newSearch:true)
    }
    
    func update(at index: Int) -> Void {
        let indexPath = IndexPath(row: index, section: 0)
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    func resultsUpdated() -> Void {
        searchResults = delegate?.youtubeSearchResults(self)
        moreSearching = false
        self.tableView.reloadData()
    }
    
    func updateFor(video videoID: String) -> Void {
        if let visible = self.tableView.indexPathsForVisibleRows {
            for indexPath in visible {
                if indexPath.row < self.searchResults?.count {
                    if self.searchResults?[indexPath.row].videoid! == videoID {
                        update(at: indexPath.row)
                    }
                }
            }
        }  // let visible
    }


    // MARK: - Table view

    // MARK: Table view delegate

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        var result: String?
        
        if let resultsCount = self.searchResults?.count {
            result = "Search Results \(resultsCount) videos"
        } else {
            result = "Search Results"
        }

        return result
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row < self.searchResults?.count {
            selectedIndex = indexPath.row
            let youtubeVideo : YoutubeVideo = (self.searchResults?[indexPath.row])!
            print("Select: \(youtubeVideo.title!) id \(youtubeVideo.videoid!)")
            if let searchResultsDelegate = delegate {
                // From Segmented controller
                tableView.deselectRow(at: indexPath, animated: true)
                searchResultsDelegate.youtubeSearchResults(self, selectedVideoAtIndex:indexPath.row)
                searchResultsDelegate.youtubeSearchResults(self, selectedVideo:youtubeVideo)
                
            } else {
                
                // Simple present of SearchTableView and associated navigaton controller
                selectedYoutubeVideo = youtubeVideo
                performSegue(withIdentifier: "JWYoutubeSearchToMP3Segue", sender:nil)
            }
            
        } else {
            if !moreSearching {
                if let searchResultsDelegate = delegate {
                    searchResultsDelegate.youtubeSearchResults(self, searchFor: searchString!, newSearch:false)
                }

            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row < self.searchResults?.count {
            let youtubeVideo : YoutubeVideo = (self.searchResults?[indexPath.row])!
            cell.textLabel!.text = youtubeVideo.title
            cell.detailTextLabel!.text = youtubeVideo.description
            
            cell.imageView!.image = delegate?.youtubeSearchResultsImageForVideo(youtubeVideo.videoid!)
        } else {

            cell.textLabel!.text = "Loading More ...";
            cell.detailTextLabel!.text = "";
            cell.imageView!.image = nil;

            if !moreSearching {
                if let queryText = searchString  {
                    moreSearching = true
                    delegate?.youtubeSearchResults(self, searchFor: queryText, newSearch:false)
                }
            }
        }
    }
    

    // MARK: Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var result: Int = 0
        if let resultsCount = self.searchResults?.count {
            if resultsCount > 0 {
                result = resultsCount + 1
            }
        }
        return result
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JWYoutubeSearchResultCell", for: indexPath)
        // Configure the cell...
        
        if indexPath.row < self.searchResults?.count {
            cell.accessoryType = .detailDisclosureButton
            cell.selectionStyle = .default
            
            var highlighted = false
            
            if let searchResultsDelegate = delegate {
                // From Segmented controller
                highlighted =  searchResultsDelegate.youtubeSearchResults(self, shouldHighlightItemAtIndex: indexPath.row)
            }
            
            if highlighted {
                let backView = UIView()
                backView.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
                cell.backgroundView = backView
            } else {
                cell.backgroundView = nil
            }
            
        } else {
            cell.accessoryType = .none
            cell.selectionStyle = .none
        }

        return cell
    }

  
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "JWYoutubeSearchToMP3Segue" {
            if let youtubeMP3ViewController = segue.destination as? JWYoutubeMP3ViewController {
                if let youtubeVideoId = selectedYoutubeVideo?.videoid {
                    let youtubeVideoLinkString = youtubeLinkWithoutId + youtubeVideoId
                    let videoTitle = selectedYoutubeVideo?.title ?? ""
                    
                    youtubeMP3ViewController.setUrlSession(youtubeVideoLinkString, videoId: youtubeVideoId, videoTitle: videoTitle)
                    //youtubeMP3ViewController.youTubeLinkURL = NSURL(string:youtubeVideoLinkString!)
                    youtubeMP3ViewController.tapToJam = true
                }
            }
        }
    }
    
    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion:{
            print("completed")
        } )
    }
    
}

