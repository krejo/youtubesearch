//
//  JWYoutubeData.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/4/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation


/**
 superclass for youtube data requests

 contains common method performGetRequestTo
 */

class JWYoutubeDataRequest {
    
    let baseURLString = "https://www.googleapis.com/youtube/v3"
    
    fileprivate let apiKey = "AIzaSyDZX_Y5M0XIp69bPqOwM0fezYpSwQ2oUdg"
 
    fileprivate func performGetRequestTo(_ targetURL:URL, completionHandler:@escaping ((_ data:Data?, _ HTTPStatusCode: Int) -> Void)) {
        
        var request = URLRequest(url: targetURL)
        
        request.httpMethod = "GET"
        
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                print("HTTP Status Code = \(httpResponse.statusCode)")
                DispatchQueue.main.async {
                    completionHandler(data, httpResponse.statusCode);
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(data, -1);
                }
            }
        }) 
        
        task.resume()
    }
    
}


/**
 JWYoutubeSearchData manages retrieval of video data Search results for a search query
 
  - submits the request
  - gets the JSON response
  - obtains info from JSON
 
  Code
  ----
 
  use method `getSearchResultsFor(`. to conduct a searc and process the results in a completion block
     Get search results
 
     youtubeSearchData.getSearchResultsFor(searchText, newSearch: newSearchResults, resultsSoFar: 0) { (results, searchString) in
 
 */

class JWYoutubeSearchData: JWYoutubeDataRequest {
    
    let useFakeResults = false
    
    fileprivate var searchResultsDicts = [[String:AnyObject]]()
    fileprivate var nextPageToken: AnyObject?
    fileprivate var searchString: String?
    fileprivate var totalResults = 0
    
    /// conducts a youtube video search a given `searchString`.
    ///
    /// Usage:
    ///
    ///    getSearchResultsFor("acdc")) // search for acdc!
    ///
    /// - parameters:
    ///   - String: A searchString
    ///   - bool: newSearch whether is a new search or continue search.
    /// - returns: Void

    func getSearchResultsFor(_ searchString:String, newSearch:Bool, resultsSoFar: Int,
                             completion:@escaping ( _ results:[[String:AnyObject]], _ searchString:String ) -> Void) {
        
        // Construct the URL String
        
        var urlComponents = URLComponents(string: baseURLString)
        
//        guard let urlComponents = URLComponents(string: baseURLString)
//            else {
//                return
//        }

        var queryItems = [
            URLQueryItem(name: "part", value: "snippet"),
            URLQueryItem(name: "q", value: searchString),
            URLQueryItem(name: "key", value: self.apiKey),
            URLQueryItem(name: "type", value: "video")
        ]

        if (newSearch) {
            nextPageToken = nil
        }
        
        if let nextToken = nextPageToken as? String {
            queryItems += [URLQueryItem(name: "pageToken", value: nextToken)]
        }
        
        urlComponents?.queryItems = queryItems

//        let msg = nextPageToken == nil ? "NEW SEARCH" : "NEXTPAGE"
//        print("_nextPageToken \(nextPageToken) \(msg)")

        self.searchString = searchString  // remember last search string
        
        var resultsSoFarComputed = resultsSoFar

        if (newSearch) {
            searchResultsDicts.removeAll()
            resultsSoFarComputed = 0
        }

        if let targetURL = urlComponents?.url?.appendingPathComponent("search") {
            
            // Submit the URL
            // Trailing closure for completionHandler
            
            performGetRequestTo(targetURL) {
                
                ( data, HTTPStatusCode ) in

                if (HTTPStatusCode == 200) {
                    
                    // class func JSONObjectWithData(_ data: NSData,options opt: NSJSONReadingOptions) throws -> AnyObject
                    
                    guard
                        let responseData = data,
                        let jsonData = try? JSONSerialization.jsonObject(with: responseData, options:[]) as! [AnyHashable: Any]
                        else {
                            return;
                    }
                    
                    let pageInfo = jsonData["pageInfo"] as! [AnyHashable : Any]
                    let resultsPerPage = pageInfo["resultsPerPage"] as! UInt
                    let lastOfPageIndex = resultsPerPage - 1
                    self.nextPageToken  = jsonData["nextPageToken"] as AnyObject?
                    
                    var counter: UInt = 0
                    let minimumResults = 10
                    let items = jsonData["items"] as! [[AnyHashable: Any] ]
                    
                    // iterate items
                    
                    for i in 0 ..< items.count {
                        
                        let itemDict = items[i]
                        let snippetDict = itemDict["snippet"] as! [AnyHashable: Any]
                       
                        // Create a new dictionary to store only the values we care about.
                        var valuesDict = [String : AnyObject]()
                        
                        valuesDict["videoid"] = (itemDict["id"] as! [AnyHashable: Any])["videoId"] as AnyObject?
                        valuesDict["title"] = snippetDict["title"] as AnyObject?
                        valuesDict["description"] = snippetDict["description"] as AnyObject?
                        valuesDict["thumbnail"] = ((snippetDict["thumbnails"] as!
                            [AnyHashable: Any])["default"] as! [AnyHashable: Any])["url"] as AnyObject?
                        
                        self.searchResultsDicts.append(valuesDict)
                        
                        // Are will still intersted in these results
                        
                        if searchString == self.searchString! {
                            
                            if (counter == lastOfPageIndex ){
                                let countResultsSoFar = resultsSoFarComputed + Int(counter) + 1
                                if countResultsSoFar < minimumResults {
                                    
                                    self.getSearchResultsFor(searchString, newSearch:false , resultsSoFar:countResultsSoFar, completion:completion)
                                } else {
                                    completion(self.searchResultsDicts,searchString)
                                    self.searchResultsDicts.removeAll()
                                }
                            }
                            
                            counter += 1
                            if (counter < resultsPerPage) {
                                continue;
                            } else {
                                break;
                            }
                            
                        } else {
                            break; // no completion
                        }
                        
                    }  // for item in
                    
                    
                } // 200
                else {
                    print("Invalid HTTPStatusCode \(HTTPStatusCode)")
                }
                
            } // trailing closure performGetRequestTo
            
            
        } // targetURL
        else {
            
            //print("Invalid URL \(urlString!)")
            
            print("Invalid URL \(urlComponents)")
        }
        
        
    }  // func
    
   
}


/**
 JWYoutubeData manages retrieval of video data for a given videoID
 
 Obtains snippet info for a single videoID
 
 - submits the request
 - gets the JSON response
 - obtains info from JSON
 
 Code
 ----
 use method `getVideoData(`. to conduct a searc and process the results in a completion block
 
     Get video info
 
     youtubeData.getVideoData(r(searchText, newSearch: newSearchResults, resultsSoFar: 0) { (results, searchString) in
 
 */

class JWYoutubeData:  JWYoutubeDataRequest {
    
    typealias videoDataCompletionMethod = ( _ result:[String: AnyObject], _ videoId:String ) -> Void
    
    /// conducts a youtube video search a given `videoId`.
    ///
    /// Usage:
    ///
    ///    getVideoData("acdfghj")) // search for acdc!
    ///
    /// - parameters:
    ///   - String: A searchString
    ///   - ( result:[String: AnyObject], videoId:String ) -> Void: completion closure
    /// - returns: Void

    func getVideoData(_ videoId:String, completion:@escaping videoDataCompletionMethod) -> Void {
        
        var urlComponents = URLComponents(string: baseURLString)
        
//        guard let urlComponents = URLComponents(string: baseURLString)
//            else {
//                return
//        }
        
        urlComponents?.queryItems = [
            URLQueryItem(name: "part", value: "snippet"),
            URLQueryItem(name: "id", value: videoId),
            URLQueryItem(name: "key", value: self.apiKey),
            URLQueryItem(name: "type", value: "video")
        ]
        
        if let targetURL = urlComponents?.url?.appendingPathComponent("videos") {
            
            // Submit the URL, Trailing closure for completionHandler
            
            performGetRequestTo(targetURL) {
                
                ( data, HTTPStatusCode ) in
                
                if (HTTPStatusCode == 200) {
                    
                    // class func JSONObjectWithData(_ data: NSData,options opt: NSJSONReadingOptions) throws -> AnyObject
                    
                    guard
                        let responseData = data,
                        let jsonData = try? JSONSerialization.jsonObject(with: responseData, options:[]) as! [AnyHashable: Any]
                        else {
                            return;
                    }
                    
                    if let items = jsonData["items"] as? [[AnyHashable: Any] ] {
                        
                        let itemDict = items.first! as [AnyHashable: Any]
                        let snippetDict = itemDict["snippet"] as! [AnyHashable: Any]
                        let videoId = itemDict["id"] as! String
                        
                        // Create a new dictionary to store only the values we care about.
                        
                        var returnItem = [String : AnyObject]()
                        returnItem["videoid"] = videoId as AnyObject?
                        returnItem["title"] = snippetDict["title"] as AnyObject?
                        returnItem["description"] = snippetDict["description"] as AnyObject?
                        returnItem["thumbnail"] = ((snippetDict["thumbnails"] as!
                            [AnyHashable: Any])["default"] as! [AnyHashable: Any])["url"] as AnyObject?
                        
                        returnItem["thumbnailsDict"] = snippetDict["thumbnails"] as! [AnyHashable: Any] as AnyObject?
                        
                        completion(returnItem, videoId)
                        
                    } else {
                        print("items un obtainable in youtube response")
                    }
                    
                } // 200
                else {
                    print("Invalid HTTPStatusCode \(HTTPStatusCode)")
                }
                
            } // trailing closure performGetRequestTo
            
        } // targetURL
        else {
            
            //print("Invalid URL \(urlString)")
            print("Invalid URL \(urlComponents)")
        }
        
        
    }  // func
    
}



/*
 Single video RESPONSE
 
 HTTP Status Code = 200
 [etag: "5g01s4-wS2b4VpScndqCYc5Y-8k/ZNoSBF0C6_12K9cOnTsPmXVDR8k", kind: youtube#videoListResponse, items: <__NSArrayM 0x7fda90768fb0>(
 {
 etag = "\"5g01s4-wS2b4VpScndqCYc5Y-8k/0HIpKyz8ME_p0lg9xEyzTnGS0hA\"";
 id = D8LTCr3BQW8;
 kind = "youtube#video";
 snippet =     {
 categoryId = 10;
 channelId = UCmPuJ2BltKsGE2966jLgCnw;
 channelTitle = acdcVEVO;
 description = "Music video by AC/DC performing Shoot To Thrill. (c) 2003 J. Albert & Sons (Pty.) Ltd.\n\nhttp://vevo.ly/yQJoWY";
 liveBroadcastContent = none;
 localized =         {
 description = "Music video by AC/DC performing Shoot To Thrill. (c) 2003 J. Albert & Sons (Pty.) Ltd.\n\nhttp://vevo.ly/yQJoWY";
 title = "AC/DC - Shoot to Thrill";
 };
 publishedAt = "2016-06-27T21:53:24.000Z";
 tags =         (
 Pop,
 Epic,
 "Shoot to Thrill",
 "AC/DC"
 );
 thumbnails =         {
 default =             {
 height = 90;
 url = "https://i.ytimg.com/vi/D8LTCr3BQW8/default.jpg";
 width = 120;
 };
 high =             {
 height = 360;
 url = "https://i.ytimg.com/vi/D8LTCr3BQW8/hqdefault.jpg";
 width = 480;
 };
 maxres =             {
 height = 720;
 url = "https://i.ytimg.com/vi/D8LTCr3BQW8/maxresdefault.jpg";
 width = 1280;
 };
 medium =             {
 height = 180;
 url = "https://i.ytimg.com/vi/D8LTCr3BQW8/mqdefault.jpg";
 width = 320;
 };
 standard =             {
 height = 480;
 url = "https://i.ytimg.com/vi/D8LTCr3BQW8/sddefault.jpg";
 width = 640;
 };
 };
 title = "AC/DC - Shoot to Thrill";
 };
 }
 )
 , pageInfo: {
 resultsPerPage = 1;
 totalResults = 1;
 }]
 */

