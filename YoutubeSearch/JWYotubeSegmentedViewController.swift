//
//  JWYotubeSegmentedViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/2/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit


class JWYotubeSegmentedViewController: UIViewController, JWYoutubeSearchResultsDelegate, JWPagingConvertPagingViewProtocol, JWYoutubeSearchResultsDataDelegate, NSCacheDelegate {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionViewContainer: UIView!
    @IBOutlet weak var tableViewContainer: UIView!
    
    fileprivate weak var tableViewSearchResultsView: JWYoutubeSearchTableViewController?
    fileprivate weak var collectionSearchResultsView: JWYoutubeCollectionViewController?
    
    fileprivate var pagingDataSource: JWPagingConvertDatasource?
    fileprivate var pagingDelegate: JWPagingConvertPagingViewDelegate?

    fileprivate let youtubeLinkWithoutId = "http://youtu.be/"
    fileprivate var selectedYoutubeVideo: YoutubeVideo?
    fileprivate var selectedYoutubeVideoIndex: Int?

    fileprivate var automatic: Bool = false
    fileprivate var pendingConvert = [Int:UIViewController]()

    var youtubeSearchData: JWYoutubeSearchResultsData?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        youtubeSearchData = JWYoutubeSearchResultsData(with: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func shouldHighlightItemAtIndex(_ index:Int) -> Bool {
        // If in pending, highlight
        if let _ = pendingConvert[index] {
            return true
        }
        return false
    }

    
    // MARK: JWYoutubeSearchResultsDelegate
    
    func youtubeSearchResults(_ youtubeSearch: AnyObject?, searchFor searchText:String, newSearch:Bool) -> Void {
        youtubeSearchData?.getYoutubeDataFor(searchText,newSearch:newSearch)
    }
    
    func youtubeSearchResultsSearchReset(_ youtubeSearch: AnyObject?) -> Void {
        pendingConvert.removeAll()  // paging doesnt need any on new search
    }

    func youtubeSearchResults(_ youtubeSearch: AnyObject?, shouldHighlightItemAtIndex index:Int) -> Bool {
        return shouldHighlightItemAtIndex(index)
    }

    func youtubeSearchResults(_ youtubeSearch: AnyObject?) -> [YoutubeVideo]? {
        return youtubeSearchData?.searchResults
    }
    
    func youtubeSearchResultsImageForVideo(_ videoId: String) -> UIImage? {
        return youtubeSearchData?.imageForVideo(videoId)
    }
    
    func youtubeSearchResults(_ youtubeSearch: AnyObject?, selectedVideoAtIndex index:Int) -> Void {
        selectedYoutubeVideoIndex = index
    }

    func youtubeSearchResults(_ youtubeSearch: AnyObject?, selectedVideo video:YoutubeVideo) -> Void {
        selectedYoutubeVideo = video
        //performSegueWithIdentifier("JWYoutubeSearchSegmentToMP3Segue", sender: nil)
        performSegue(withIdentifier: "JWYoutubeSearchSegmentToPagingMP3Segue", sender: nil)
    }
    
    func youtubeSearchResults(_ controller: AnyObject?, selectedVideo video:YoutubeVideo, automatic:Bool) -> Void {
        self.automatic = automatic
        youtubeSearchResults(controller, selectedVideo:video)
    }

    func youtubeSearchResults(_ youtubeSearch: AnyObject?, selectedVideo video:YoutubeVideo, atIndex index:Int) -> Void {
        // Not used yet
        selectedYoutubeVideo = video
        selectedYoutubeVideoIndex = index
        //performSegueWithIdentifier("JWYoutubeSearchSegmentToMP3Segue", sender: nil)
        performSegue(withIdentifier: "JWYoutubeSearchSegmentToPagingMP3Segue", sender: nil)
    }


    // MARK: JWPagingConvertPagingViewProtocol
    
    func pagingView(_ controller:JWPagingConvertPagingViewDelegate?, viewControllerPending:UIViewController?, atIndex index:Int) -> Void
    {
        pendingConvert[index] = viewControllerPending
        tableViewSearchResultsView?.update(at: index)
    }
    

    // MARK: JWYoutubeSearchResultsDataDelegate
    
    func searchResultsData(_ youtubeSearch: JWYoutubeSearchResultsData, pendingViewControllerForItemAtIndex index:Int) -> UIViewController? {
        
        var result: UIViewController?
        if let pendingViewController = pendingConvert[index] {
            result = pendingViewController
        }
        
        return result
    }
    
    func searchResultsData(_ youtubeSearch: JWYoutubeSearchResultsData, updateAtIndex index: Int) -> Void {
        tableViewSearchResultsView?.update(at: index)
    }
    
    func searchResultsDataUpdated(_ youtubeSearch: JWYoutubeSearchResultsData) -> Void {
        tableViewSearchResultsView?.resultsUpdated()
    }
    
    func searchResultsData(_ youtubeSearch: JWYoutubeSearchResultsData, updateFor videoID: String) -> Void {
        tableViewSearchResultsView?.updateFor(video: videoID)
    }
    
    func searchResultsData(_ youtubeSearch: JWYoutubeSearchResultsData, updateWithResults results: [YoutubeVideo]?) -> Void {
        tableViewSearchResultsView?.resultsUpdated()
    }

    
    // MARK: - Navigation
    
    @IBAction func segmentChanged(_ sender: AnyObject) {
        
        let sControl = sender as! UISegmentedControl
        if sControl.selectedSegmentIndex == 0 {
            
            if let topIndex = collectionSearchResultsView?.topItemIndex() {
                tableViewSearchResultsView?.focusOnIndex(topIndex, animated: false)
            }
            
            collectionViewContainer.isHidden = true
            tableViewContainer.isHidden = false
            
        } else {
            collectionSearchResultsView!.activated()
            
            if let topIndex = tableViewSearchResultsView?.topItemIndex() {
                collectionSearchResultsView?.focusOnIndex(topIndex, animated: false)
            }
            
            collectionViewContainer.isHidden = false
            tableViewContainer.isHidden = true
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "JWEmbedTableViewSearchResults" {
            if let resultsTableView = segue.destination as? JWYoutubeSearchTableViewController {
                resultsTableView.delegate = self
                tableViewSearchResultsView = resultsTableView
            }
        }
        else if segue.identifier == "JWEmbedCollectionViewSearchResults" {
            if let collectionView = segue.destination as? JWYoutubeCollectionViewController {
                collectionView.delegate = self
                collectionSearchResultsView = collectionView
            }
        }
        else if segue.identifier == "JWYoutubeSearchSegmentToMP3Segue" {
            
            if let youtubeMP3ViewController = segue.destination as? JWYoutubeMP3ViewController {
                
                guard let youtubeVideoId = selectedYoutubeVideo!.videoid,
                    let youtubeVideoTitle = selectedYoutubeVideo!.title
                    else {
                        return
                }
                
                let youtubeVideoLinkString = youtubeLinkWithoutId + youtubeVideoId
                
                youtubeMP3ViewController.setUrlSession(youtubeVideoLinkString, videoId: youtubeVideoId, videoTitle: youtubeVideoTitle)
                youtubeMP3ViewController.automatic = automatic
                youtubeMP3ViewController.tapToJam = true
                youtubeMP3ViewController.startingCoverImage = youtubeSearchData?.imageForVideo(youtubeVideoId)
            }
        }
        else if segue.identifier == "JWYoutubeSearchSegmentToPagingMP3Segue" {
            
            if let pagingYoutubeMP3ViewController = segue.destination as? UIPageViewController {
                
                pagingDelegate = JWPagingConvertPagingViewDelegate()
                pagingDelegate?.delegate = self
                
                pagingDataSource = JWPagingConvertDatasource()
                pagingDataSource?.delegate = youtubeSearchData
                
                pagingYoutubeMP3ViewController.dataSource = pagingDataSource
                pagingYoutubeMP3ViewController.delegate = pagingDelegate
                
                var startingIndex = 0
                if let selectedIndex = selectedYoutubeVideoIndex {
                    startingIndex = selectedIndex
                }
                
                let startingViewController = pagingDataSource?.viewControllerAtIndex(startingIndex)
                
                if let youtubeMP3ViewController = startingViewController as? JWYoutubeMP3ViewController {
                    youtubeMP3ViewController.automatic = automatic
                }
                // Turn off temporary automatic after it is applied
                automatic = false
                
                
                pagingYoutubeMP3ViewController.setViewControllers([startingViewController!], direction: .forward, animated: false, completion: nil)
                
                pagingYoutubeMP3ViewController.view.backgroundColor = UIColor.darkGray
            }
        }
        
    }

}
