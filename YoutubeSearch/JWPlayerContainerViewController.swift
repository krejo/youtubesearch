//
//  JWPlayerContainerViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class JWPlayerContainerViewController: UIViewController {

    var audioFileURL: URL?
    fileprivate var avPlayer: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        avPlayer?.pause()
        avPlayer = nil
    }
    
    func play() {
        avPlayer?.play()
    }
    
    func pause() {
        avPlayer?.pause()
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "JWEmbedAVPlayer" {
            avPlayer = AVPlayer(url: self.audioFileURL!)
            
            if let playerViewController = segue.destination as? AVPlayerViewController {
            
                playerViewController.player = avPlayer
                playerViewController.showsPlaybackControls = true
                
                //avPlayer!.play()
            }
           
        }
    }

}
