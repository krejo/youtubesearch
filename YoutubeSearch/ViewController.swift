//
//  ViewController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/29/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    fileprivate var firstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        firstTime = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if firstTime {
            self.performSegue(withIdentifier: "JWPresentYoutubeSearch", sender: self)
            firstTime = false
        }
    }

    /*
     In the storyboard main in the reference to YoutubeMain use one of these two starting points
     JWYoutubeSearchNC
     JWYoutubeSearchSegmentedNC
 */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JWPresentYoutubeSearch" {
            print("\(#function)")
        } 
    }


}

