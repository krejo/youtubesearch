//
//  JWPagingController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/5/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

protocol JWPagingConvertDelegate: class {
    func paging(_ controller:JWPagingConvertDatasource?, itemAtIndex index:Int) -> YoutubeVideo?
    func pagingNumberOfPages(_ controller:JWPagingConvertDatasource?) -> Int
    func paging(_ controller:JWPagingConvertDatasource?, imageForItemAtIndex index:Int) -> UIImage?
    func paging(_ controller:JWPagingConvertDatasource?, pendingViewControllerForItemAtIndex index:Int) -> UIViewController?
}


/**
 JWPagingConvertDatasource conforms to UIPageViewControllerDataSource
 
 It has a delegate to obtain various bits of information
 
 */

class JWPagingConvertDatasource: NSObject, UIPageViewControllerDataSource {

    fileprivate let youtubeLinkWithoutId = "http://youtu.be/"

    weak var delegate: JWPagingConvertDelegate?

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        //print("\(#function)")
        var result: UIViewController?
        if let sourcePage = (viewController as! JWYoutubeMP3ViewController).pageIndex {
            if (sourcePage > 0) {
                result = viewControllerAtIndex(sourcePage - 1)
            }
        }
        
        return result;
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                              viewControllerAfter viewController: UIViewController) -> UIViewController? {

        //print("\(#function)")
        var result: UIViewController?
        
        if let numberOfPages = delegate?.pagingNumberOfPages(self) {
            if let sourcePage = (viewController as! JWYoutubeMP3ViewController).pageIndex {
                if (sourcePage < numberOfPages - 1) {
                    result = viewControllerAtIndex(sourcePage + 1)
                }
            }
        }
        
        return result
    }

    
    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        print("\(#function) \(index)")

        var result: UIViewController?
        
        if let pendingViewController = delegate?.paging(self, pendingViewControllerForItemAtIndex: index) {
            
            pendingViewController.view.layoutIfNeeded()
            result = pendingViewController
            
        } else {
            
            let youtubeMP3ViewController = UIStoryboard(name: "YoutubeMain",
                bundle:nil).instantiateViewController(withIdentifier: "JWYoutubeConvertToMP3ViewController") as! JWYoutubeMP3ViewController
            
            if let youtubeVideo = delegate?.paging(self, itemAtIndex: index) {
                
                youtubeMP3ViewController.pageIndex = index
                
                guard let youtubeVideoId = youtubeVideo.videoid,
                    let youtubeVideoTitle = youtubeVideo.title
                    else {
                        return nil
                }
                
                let youtubeVideoLinkString = youtubeLinkWithoutId + youtubeVideoId
                
                youtubeMP3ViewController.setUrlSession(youtubeVideoLinkString, videoId: youtubeVideoId, videoTitle: youtubeVideoTitle)
                
                //youtubeMP3ViewController.youTubeLinkURL = NSURL(string:youtubeVideoLinkString)
                youtubeMP3ViewController.tapToJam = true
                youtubeMP3ViewController.automatic = false
                youtubeMP3ViewController.startingCoverImage = delegate?.paging(self, imageForItemAtIndex: index)
            }
            
            result = youtubeMP3ViewController
        }
        
        return result
        
    }
    
    
}



protocol JWPagingConvertPagingViewProtocol: class {
    func pagingView(_ controller:JWPagingConvertPagingViewDelegate?, viewControllerPending:UIViewController?, atIndex index:Int) -> Void
}

/**
 JWPagingConvertPagingViewDelegate conforms to UIPageViewControllerDelegate
 
 It has a delegate to notify about various things
 JWPagingConvertPagingViewProtocol
 
 will be alerted when a viewController in the paging sequence has a convert in progress
 */

class JWPagingConvertPagingViewDelegate: NSObject, UIPageViewControllerDelegate {

    fileprivate var lastPendingIndex: Int?
    
    weak var delegate: JWPagingConvertPagingViewProtocol?

    func pageViewController(_ pageViewController: UIPageViewController,
                            willTransitionTo pendingViewControllers: [UIViewController]) {

        if let youtubeMP3ViewController = pageViewController.viewControllers?.first as? JWYoutubeMP3ViewController, youtubeMP3ViewController.convertUserActivated {
                
                if let pageIndex = youtubeMP3ViewController.pageIndex {
                    print("\(#function) transitioning from \(pageIndex)")
                    delegate?.pagingView(self, viewControllerPending:youtubeMP3ViewController, atIndex: pageIndex)
                    lastPendingIndex = pageIndex
                }
        }

        
        
    }

}

