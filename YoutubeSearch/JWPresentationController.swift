//
//  JWPresentationController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit



/**
 A delegate protocol for our presentation controller
 
 */

protocol JWPresentationControllerDelegate: class {
    func presentationControllerDidDismiss(_ controller: JWPresentationController)
}

/**
 Presentationcontroller to display to preferred content size
 
 */

class JWPresentationController: UIPresentationController, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    
    var useRounded:Bool = false
    var offsetFromBottom:CGFloat = 0.0
    
    weak var  delegateController: JWPresentationControllerDelegate?
    
    fileprivate var dimmingView: UIView?
    fileprivate var wrapperView: UIView?
    fileprivate let cornerRadius: CGFloat = 12.0
    
    
    // MARK: Lifeycle
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)

        presentedViewController.modalPresentationStyle = .custom
    }
    
    override var presentedView : UIView? {
        return wrapperView
    }
    
    func configurePresentationView() {
        
        // The default implementation of -presentedView returns
        // self.presentedViewController.view.
        
        let presentedViewControllerView = super.presentedView
        
        // Wrap the presented view controller's view in an intermediate hierarchy
        
        let presentationWrapperView = UIView(frame:self.frameOfPresentedViewInContainerView)
        presentationWrapperView.layer.shadowOpacity = 0.44
        presentationWrapperView.layer.shadowRadius = 10.0
        presentationWrapperView.layer.shadowOffset = CGSize(width: 0, height: -4.0)
        
        self.wrapperView = presentationWrapperView;

        if useRounded {
            // Wrap the presented view controller's view in an intermediate hierarchy
            // that applies a shadow and rounded corners to the top-left and top-right
            // edges.  The final effect is built using three intermediate views.
            //
            // presentationWrapperView              <- shadow
            //   |- presentationRoundedCornerView   <- rounded corners (masksToBounds)
            //        |- presentedViewControllerWrapperView
            //             |- presentedViewControllerView (presentedViewController.view)
            //

            // presentationRoundedCornerView is CORNER_RADIUS points taller than the
            // height of the presented view controller's view.  This is because
            // the cornerRadius is applied to all corners of the view.  Since the
            // effect calls for only the top two corners to be rounded we size
            // the view such that the bottom CORNER_RADIUS points lie below
            // the bottom edge of the screen.
            
            let frameRoundedCorner =  UIEdgeInsetsInsetRect(presentationWrapperView.bounds, UIEdgeInsetsMake(0, 0, -cornerRadius, 0))
            
            let presentationRoundedCornerView = UIView(frame:frameRoundedCorner)
            presentationRoundedCornerView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            presentationRoundedCornerView.layer.cornerRadius = cornerRadius
            presentationRoundedCornerView.layer.masksToBounds = true

            // To undo the extra height added to presentationRoundedCornerView,
            // presentedViewControllerWrapperView is inset by CORNER_RADIUS points.
            // This also matches the size of presentedViewControllerWrapperView's
            // bounds to the size of -frameOfPresentedViewInContainerView.

            let frameWrapperView = UIEdgeInsetsInsetRect(presentationRoundedCornerView.bounds, UIEdgeInsetsMake(0, 0, cornerRadius, 0))
            
            let presentedViewControllerWrapperView = UIView(frame:frameWrapperView)
            presentedViewControllerWrapperView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            
            presentedViewControllerView!.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            presentedViewControllerView!.frame = presentedViewControllerWrapperView.bounds;

            // Add presentedViewControllerView -> presentedViewControllerWrapperView.
            presentedViewControllerWrapperView.addSubview(presentedViewControllerView!)
            
            // Add presentedViewControllerWrapperView -> presentationRoundedCornerView.
            presentationRoundedCornerView.addSubview(presentedViewControllerWrapperView)
            
            // Add presentationRoundedCornerView -> presentationWrapperView.
            self.wrapperView!.addSubview(presentationRoundedCornerView)

        }
        else {
            
            // Wrap the presented view controller's view in an intermediate hierarchy
            // that applies a shadow 
            // The final effect is built using two intermediate views.
            //
            // presentationWrapperView              <- shadow
            //        |- presentedViewControllerWrapperView
            //             |- presentedViewControllerView (presentedViewController.view)

            let frameWrapperView = self.wrapperView!.bounds
            
            let presentedViewControllerWrapperView = UIView(frame:frameWrapperView)
            presentedViewControllerWrapperView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            
            presentedViewControllerView!.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            presentedViewControllerView!.frame = presentedViewControllerWrapperView.bounds;

            // Add presentedViewControllerView -> presentedViewControllerWrapperView.
            presentedViewControllerWrapperView.addSubview(presentedViewControllerView!)
            
            // Add presentationRoundedCornerView -> presentationWrapperView.
            self.wrapperView!.addSubview(presentedViewControllerWrapperView)
        }
        
        
        // Add a dimming view behind presentationWrapperView.  self.presentedView
        // is added later (by the animator) so any views added here will be
        // appear behind the -presentedView.
        
        let dimmingView = UIView(frame: self.containerView!.bounds)
        dimmingView.backgroundColor = UIColor.black
        dimmingView.isOpaque = false
        dimmingView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        dimmingView.alpha = 0.0
        dimmingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped)))
        
        self.dimmingView = dimmingView;
        
        self.containerView!.addSubview(dimmingView)
    }
    

    // From Apple CustomTransitions
    
    //  At WillBegin the containerView has been created and the view hierarchy set up for the
    //  presentation.  However, the -presentedView has not yet been retrieved.
    
    override func presentationTransitionWillBegin () {
        
        configurePresentationView()
        
        // Get the transition coordinator for the presentation so we can
        // fade in the dimmingView alongside the presentation animation.
        
        if let transitionCoordinator = self.presentingViewController.transitionCoordinator {
            
            transitionCoordinator.animate(alongsideTransition: {
                (contex) in
                self.dimmingView!.alpha = 0.45
                
                },completion: nil
            )
        }
        
    }
    
    override func presentationTransitionDidEnd(_ completed:Bool) {
        // The value of the 'completed' argument is the same value passed to the
        // -completeTransition: method by the animator.  It may
        // be NO in the case of a cancelled interactive transition.
        if !completed {
            // The system removes the presented view controller's view from its
            // superview and disposes of the containerView.  This implicitly
            // removes the views created in -presentationTransitionWillBegin: from
            // the view hierarchy.  However, we still need to relinquish our strong
            // references to those view.
            self.wrapperView = nil
            self.dimmingView = nil
        }
    }
    
    override func dismissalTransitionWillBegin() {
        // Get the transition coordinator for the dismissal so we can
        // fade out the dimmingView alongside the dismissal animation.
        
        if let transitionCoordinator = self.presentingViewController.transitionCoordinator {
            
            transitionCoordinator.animate(alongsideTransition: {
                (contex) in
                self.dimmingView!.alpha = 0.0
                
                }, completion: nil)
        }
        
    }
    
    override func dismissalTransitionDidEnd(_ completed:Bool){
        // The value of the 'completed' argument is the same value passed to the
        // -completeTransition: method by the animator.  It may
        // be NO in the case of a cancelled interactive transition.
        
        if completed {
            // The system removes the presented view controller's view from its
            // superview and disposes of the containerView.  This implicitly
            // removes the views created in -presentationTransitionWillBegin: from
            // the view hierarchy.  However, we still need to relinquish our strong
            // references to those view.
            self.wrapperView = nil
            self.dimmingView = nil
        }
    }
    
    func dimmingViewTapped(_ sender:UITapGestureRecognizer) {
        
        delegateController?.presentationControllerDidDismiss(self)
        self.presentingViewController.dismiss(animated: true, completion:nil)
    }
    
    
    // MARK: Layout
    
    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        
        super.preferredContentSizeDidChange(forChildContentContainer: container)
    
        if let containerViewController = container as? UIViewController, containerViewController == self.presentedViewController {
            self.containerView?.setNeedsLayout()
        }
        
    }
    
    //  When the presentation controller receives a
    //  -viewWillTransitionToSize:withTransitionCoordinator: message it calls this
    //  method to retrieve the new size for the presentedViewController's view.
    //  The presentation controller then sends a
    //  -viewWillTransitionToSize:withTransitionCoordinator: message to the
    //  presentedViewController with this size as the first argument.
    //
    //  Note that it is up to the presentation controller to adjust the frame
    //  of the presented view controller's view to match this promised size.
    //  We do this in -containerViewWillLayoutSubviews.
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {

        if let containerViewController = container as? UIViewController, containerViewController == self.presentedViewController {
            return containerViewController.preferredContentSize
        }

        return super.size(forChildContentContainer: container, withParentContainerSize:parentSize)
    }
    
    override var frameOfPresentedViewInContainerView : CGRect {
        
        let containerViewBounds = self.containerView!.bounds
        
        let presentedViewContentSize = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize:containerViewBounds.size)
        
        // The presented view extends presentedViewContentSize.height points from
        // the bottom edge of the screen.
        var presentedViewControllerFrame = containerViewBounds;
        
        presentedViewControllerFrame.size.height = presentedViewContentSize.height
        presentedViewControllerFrame.origin.y = containerViewBounds.maxY - presentedViewContentSize.height - offsetFromBottom
        
        return presentedViewControllerFrame;
    }
    
    
    //  Similar to the -viewWillLayoutSubviews method
    
    override func containerViewWillLayoutSubviews() {
        print(#function)
        
        super.containerViewWillLayoutSubviews()
    
        self.dimmingView!.frame = self.containerView!.bounds
        self.wrapperView!.frame = self.frameOfPresentedViewInContainerView
    }

    
    // MARK: UIViewControllerAnimatedTransitioning
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if let context = transitionContext {
            return context.isAnimated ? 0.25 : 0
        }
        return 0
    }
    
    //  The presentation animation is tightly integrated with the overall
    //  presentation so it makes the most sense to implement
    //  <UIViewControllerAnimatedTransitioning> in the presentation controller
    //  rather than in a separate object.
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        print(#function)
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)

        let containerView = transitionContext.containerView
        
        let isPresenting: Bool = fromVC == self.presentingViewController
        
        //let fromViewInitialFrame = transitionContext.initialFrameForViewController(fromVC!)
        var fromViewFinalFrame = transitionContext.finalFrame(for: fromVC!)
        
        var toViewInitialFrame = transitionContext.initialFrame(for: toVC!)
        let toViewFinalFrame = transitionContext.finalFrame(for: toVC!)
        
        if isPresenting {
            containerView.addSubview(toView!)
        }
        
        if (isPresenting) {
            toViewInitialFrame.origin = CGPoint(x: containerView.bounds.minX, y: containerView.bounds.maxY)
            toViewInitialFrame.size = toViewFinalFrame.size
            
            toView!.frame = toViewInitialFrame
        } else {
            // Because our presentation wraps the presented view controller's view
            // in an intermediate view hierarchy, it is more accurate to rely
            // on the current frame of fromView than fromViewInitialFrame as the
            // initial frame (though in this example they will be the same).
            fromViewFinalFrame = fromView!.frame.offsetBy(dx: 0, dy: fromView!.frame.height)
        }
        
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0,usingSpringWithDamping: 3.68, initialSpringVelocity: 0.2, options: [], animations: {
            
            if isPresenting {
                toView!.frame = toViewFinalFrame
            }
            else {
                fromView!.frame = fromViewFinalFrame;
            }
            
            },completion: {
                
                (fini: Bool) in
                
                let wasCancelled = transitionContext.transitionWasCancelled
                transitionContext.completeTransition(!wasCancelled)  // Cancelled is NOT completed
        })

    }
    
    // NOTE: UITransitionContextFromViewKey
    // If NO is returned from -shouldRemovePresentersView, the view associated
    // with UITransitionContextFromViewKey is nil during presentation.  This
    // intended to be a hint that your animator should NOT be manipulating the
    // presenting view controller's view.  For a dismissal, the -presentedView
    // is returned.
    //
    // Why not allow the animator manipulate the presenting view controller's
    // view at all times?  First of all, if the presenting view controller's
    // view is going to stay visible after the animation finishes during the
    // whole presentation life cycle there is no need to animate it at all — it
    // just stays where it is.  Second, if the ownership for that view
    // controller is transferred to the presentation controller, the
    // presentation controller will most likely not know how to layout that
    // view controller's view when needed, for example when the orientation
    // changes, but the original owner of the presenting view controller does.
    
    

    
    // MARK: UIViewControllerTransitioningDelegate
    
    //  If the modalPresentationStyle of the presented view controller is
    //  UIModalPresentationCustom, the system calls this method on the presented
    //  view controller's transitioningDelegate to retrieve the presentation
    //  controller that will manage the presentation.  
    //
    //  return nil, to have an instance of UIPresentationController is used.
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController?
    {
        return self
    }
    //        NSAssert(self.presentedViewController == presented, @"You didn't initialize %@ with the correct presentedViewController.  Expected %@, got %@.",
    //                 self, presented, self.presentedViewController);

    
    //  The system calls this method on the presented view controller's
    //  transitioningDelegate to retrieve the animator object used for animating
    //  the presentation/dismissal of the incoming/outgoing view controller.  Your implementation is
    //
    //  nil if the default presentation animation should be used.
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }

}


