//
//  JWMiddlePresentationController.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/19/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

/**
 A delegate protocol for our presentation controller
 
 */

protocol JWMiddlePresentationControllerDelegate: class {
    func presentationControllerDidDismiss(_ controller: JWMiddlePresentationController)
}

/**
 Presentationcontroller to displayin middle of screen on larger screens
 
 As the _TransitioningDelegate it simply returns itself when asked for the presenationController
 Also, while conforming to _AnimatedTransitioning it also returns itself for
 both dismissal and presenation
 
 */
class JWMiddlePresentationController: UIPresentationController, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {

    weak var  delegateController: JWMiddlePresentationControllerDelegate?
    
    fileprivate var dismissButton: UIButton?
    fileprivate var wrapperView: UIView?
    
    
    // MARK: Lifeycle

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        presentedViewController.modalPresentationStyle = .custom
    }
    
    override var presentedView : UIView? {
        return wrapperView
    }
    
    // From Apple CustomTransitions

    //  This is one of the first methods invoked on the presentation controller
    //  at the start of a presentation.  By the time this method is called,
    //  the containerView has been created and the view hierarchy set up for the
    //  presentation.  However, the -presentedView has not yet been retrieved.

    override func presentationTransitionWillBegin () {
        
        configurePresentationView()
    }
    
    func configurePresentationView() {
        
        // The default implementation of -presentedView returns
        // self.presentedViewController.view.
        let presentedViewControllerView = super.presentedView
        
        // Wrap the presented view controller's view in an intermediate hierarchy
        // that applies a shadow and adds a dismiss button to the top left corner.
        //
        // presentationWrapperView              <- shadow
        //     |- presentedViewControllerView (presentedViewController.view)
        //     |- close button
        
        let presentationWrapperView = UIView(frame:CGRect.zero)
        presentationWrapperView.layer.shadowOpacity = 0.44
        presentationWrapperView.layer.shadowRadius = 10.0
        presentationWrapperView.layer.shadowOffset = CGSize(width: 0, height: -4.0)
        presentationWrapperView.backgroundColor = UIColor.clear

        presentedViewControllerView!.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        presentedViewControllerView!.layer.borderColor = UIColor.gray.cgColor;
        presentedViewControllerView!.layer.borderWidth = 2.0
        
        // Add presentedViewControllerView -> presentedViewControllerWrapperView.
        presentationWrapperView.addSubview(presentedViewControllerView!)
 
        let dismissButton = UIButton(type: .custom)
        dismissButton.frame = CGRect(x: 0, y: 0, width: 26.0, height: 26.0)
        dismissButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        dismissButton.setImage(UIImage(named:"closebutton_apple"), for: UIControlState())
        self.dismissButton = dismissButton
        
        presentationWrapperView.addSubview(self.dismissButton!)
        
        self.wrapperView = presentationWrapperView;
        
    }
    
    // Create the dismiss button.
    
    func dismissButtonTapped(_ sender: UIButton) {
        delegateController?.presentationControllerDidDismiss(self)
        self.presentingViewController.dismiss(animated: true, completion:nil)
    }
    
    
    // MARK: Layout
    
    //  This method is invoked when the interface rotates.  For performance,
    //  the shadow on presentationWrapperView is disabled for the duration
    //  of the rotation animation.
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    
        self.wrapperView?.clipsToBounds = true
        self.wrapperView?.layer.shadowOpacity = 0.0
        self.wrapperView?.layer.shadowRadius = 0.0
    
        coordinator.animate(alongsideTransition: {
            context in
            // blank
        }){
            context in
            self.wrapperView?.clipsToBounds = false
            self.wrapperView?.layer.shadowOpacity = 0.63
            self.wrapperView?.layer.shadowRadius = 17.0
        }
    }
    
    
    //  When the presentation controller receives a
    //  -viewWillTransitionToSize:withTransitionCoordinator: message it calls this
    //  method to retrieve the new size for the presentedViewController's view.
    //  The presentation controller then sends a
    //  -viewWillTransitionToSize:withTransitionCoordinator: message to the
    //  presentedViewController with this size as the first argument.
    //
    //  Note that it is up to the presentation controller to adjust the frame
    //  of the presented view controller's view to match this promised size.
    //  We do this in -containerViewWillLayoutSubviews.
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        
        if let containerViewController = container as? UIViewController, containerViewController == self.presentedViewController {
            return CGSize(width: parentSize.width/2, height: parentSize.height/2)
        }
        
        return super.size(forChildContentContainer: container, withParentContainerSize:parentSize)
    }

    
    override var frameOfPresentedViewInContainerView : CGRect {
        
        let containerViewBounds = self.containerView!.bounds
        
        let presentedViewContentSize = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize:containerViewBounds.size)
        
        // Center the presentationWrappingView view within the container.
        let presentedViewControllerFrame =
            CGRect(x: containerViewBounds.midX - presentedViewContentSize.width/2,
                       y: containerViewBounds.midY - presentedViewContentSize.height/2,
                       width: presentedViewContentSize.width, height: presentedViewContentSize.height)
        
        return presentedViewControllerFrame.insetBy(dx: -20, dy: -20);
    }
    
    
    //  Similar to the -viewWillLayoutSubviews method
    
    override func containerViewWillLayoutSubviews() {
        print(#function)
        
        super.containerViewWillLayoutSubviews()
        
        if let wrapView = wrapperView {

            wrapView.frame = self.frameOfPresentedViewInContainerView
            
            // Undo the outset that was applied in -frameOfPresentedViewInContainerView.
            presentedViewController.view.frame = wrapView.bounds.insetBy(dx: 20, dy: 20);
            
            // Place dismissButton top-left corner of the presented view controller's view.
            
            self.dismissButton?.center =
                CGPoint(x: self.presentedViewController.view.frame.minX,
                            y: self.presentedViewController.view.frame.minY)
        }
    }
    
    
    // MARK: UIViewControllerAnimatedTransitioning
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if let context = transitionContext {
            return context.isAnimated ? 0.25 : 0
        }
        return 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        print(#function)
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)

        // For a Presentation:
        //      fromView = The presenting view.
        //      toView   = The presented view.
        // For a Dismissal:
        //      fromView = The presented view.
        //      toView   = The presenting view.

        let containerView = transitionContext.containerView
        
        let isPresenting: Bool = fromVC == self.presentingViewController
        
        // (will have no effect on dismissal because the presenting view controller's view was not removed)
        // still, only do on presentation
        if isPresenting {
            if let tv = toView {
                containerView.addSubview(tv)
            }
        }
        
        if (isPresenting) {
            toView?.alpha = 0.0
            // This animation only affects the alpha.  The views can be set to
            // their final frames now.
            fromView?.frame = transitionContext.finalFrame(for: fromVC!)
            toView?.frame = transitionContext.finalFrame(for: toVC!)
            
        } else {
            // Because our presentation wraps the presented view controller's view
            // in an intermediate view hierarchy, it is more accurate to rely
            // on the current frame of fromView than fromViewInitialFrame as the
            // initial frame.
            toView?.frame = transitionContext.finalFrame(for: toVC!)
        }
        
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0,usingSpringWithDamping: 3.68, initialSpringVelocity: 0.2, options: [], animations: {
            
            if (isPresenting) {
                toView?.alpha = 1.0
            }
            else {
                fromView?.alpha = 0.0
            }
            
        }){
            (fini: Bool) in
            
            let wasCancelled = transitionContext.transitionWasCancelled
            transitionContext.completeTransition(!wasCancelled)
            
            if !isPresenting {
                fromView!.alpha = 1.0
            }
        }
        
    }
    
    // NOTE: UITransitionContextFromViewKey
    // If NO is returned from -shouldRemovePresentersView, the view associated
    // with UITransitionContextFromViewKey is nil during presentation.  This
    // intended to be a hint that your animator should NOT be manipulating the
    // presenting view controller's view.  For a dismissal, the -presentedView
    // is returned.
    //
    // Why not allow the animator manipulate the presenting view controller's
    // view at all times?  First of all, if the presenting view controller's
    // view is going to stay visible after the animation finishes during the
    // whole presentation life cycle there is no need to animate it at all — it
    // just stays where it is.  Second, if the ownership for that view
    // controller is transferred to the presentation controller, the
    // presentation controller will most likely not know how to layout that
    // view controller's view when needed, for example when the orientation
    // changes, but the original owner of the presenting view controller does.


    
    // MARK: UIViewControllerTransitioningDelegate
    
    //  If the modalPresentationStyle of the presented view controller is
    //  UIModalPresentationCustom, the system calls this method on the presented
    //  view controller's transitioningDelegate to retrieve the presentation
    //  controller that will manage the presentation.
    //
    //  return nil, to have an instance of UIPresentationController is used.
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController?
    {
        return self
    }
    
    //        NSAssert(self.presentedViewController == presented, @"You didn't initialize %@ with the correct presentedViewController.  Expected %@, got %@.",
    //                 self, presented, self.presentedViewController);
    
    //  The system calls this method on the presented view controller's
    //  transitioningDelegate to retrieve the animator object used for animating
    //  the presentation/dismissal of the incoming/outgoing view controller.  Your implementation is
    //  nil if the default presentation animation should be used.
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
}



