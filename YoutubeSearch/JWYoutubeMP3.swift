//
//  JWYoutubeMP3.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 6/30/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

/*
 From ObjC JWURLManip
 */

import UIKit

protocol JWYoutubeMP3DotOrgDelegate: class {
    func youtubeMP3DotOrg(_ controller:JWYoutubeMP3DotOrg, didObtainLink linkString:String) -> Void
    func youtubeMP3DotOrgModifiedHTMLWebDataAvailable(_ controller:JWYoutubeMP3DotOrg) -> Void
    func youtubeMP3DotOrgFailedToProcessWebData(_ controller:JWYoutubeMP3DotOrg) -> Void
}


class JWYoutubeMP3DotOrg : NSObject, URLSessionDelegate, URLSessionTaskDelegate {
    
    // MARK: Private
    
    fileprivate var session:Foundation.URLSession?
    fileprivate var dataTaskSession:URLSessionDataTask?
    fileprivate var downloadLinkStr: String?
    fileprivate var downloadOperationQueue: OperationQueue? = OperationQueue()
    fileprivate var accumulatedWebData: NSMutableData? = NSMutableData()

    // MARK: Public

    weak var delegate: JWYoutubeMP3DotOrgDelegate?

    var modifiedHTMLWebData: Data?
    var youTubeURLReplaceString:String?
    var dbkey: String?  // used by YoutubeMP3 to save the file correctly
    var youTubeLinkURLStr: String?
    var audioConverterURL: URL = URL(string: "http://www.youtube-mp3.org")!
    
    override init() {
        downloadOperationQueue!.qualityOfService = .userInitiated

        super.init()
        
        session = Foundation.URLSession(configuration: URLSessionConfiguration.default,delegate: self,
                               delegateQueue:downloadOperationQueue! )
    }
    
    convenience init(replaceString: String,delegate:JWYoutubeMP3DotOrgDelegate, dbkey: String) {
        self.init()
        self.delegate = delegate
        self.dbkey = dbkey
        self.youTubeURLReplaceString = replaceString
    }
    
    // MARK: Public API
    
    func startWebSession () {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        downloadLinkStr = nil
        dataTaskSession = session?.dataTask(with: self.audioConverterURL);
        dataTaskSession?.resume()
    }
    
    func hasDownloadLink(_ webView: UIWebView) -> Bool {
    
        var result = false
        
        if downloadLinkStr == nil {
            let javaScript = "" +
                "var script = document.createElement('script');" +
                "script.type = 'text/javascript';" +
                "script.text = " +
                "\"function getLink()" +
                "{ " +
                "  var submitter = document.getElementsByTagName('a'); " +
                "  var theLink = submitter[0]; " +
                "  for (var i = 0; i < submitter.length; i++) {" +
                "    var submitterString = submitter[i].toString();" +
                "    if (submitter[i].style.display === '' && submitterString.length > 80) {" +
                "      theLink = submitterString;" +
                "    }" +
                "  }" +
                "  return theLink;" +
                "}\";" +
                "document.getElementsByTagName('head')[0].appendChild(script);" +
            ""
            
            webView.stringByEvaluatingJavaScript(from: javaScript)
            
            if let downloadLink = webView.stringByEvaluatingJavaScript(from: "getLink();") {
                
                if downloadLink.lengthOfBytes(using: String.Encoding.utf8) > 0 {
                    downloadLinkStr = downloadLink
                    
                    delegate?.youtubeMP3DotOrg(self, didObtainLink:downloadLink)
                    
                    result = true
                }
            }
            
        } else {
            result = true
        }
    
        return result
    }

    
    //  "window.alert(submitter[i]);"
    
    
    func submitConvert(_ webView: UIWebView) {
        let javaScript = "" +
            "var script = document.createElement('script');" +
            "script.type = 'text/javascript';" +
            "script.text = \"function submitFunction() {" +
            "  var submitter = document.getElementById('submit');" +
            "  submitter.click();" +
            "}\";" +
            "document.getElementsByTagName('head')[0].appendChild(script);" +
        ""
        
        webView.stringByEvaluatingJavaScript(from: javaScript)
        webView.stringByEvaluatingJavaScript(from: "submitFunction();")
        
    }
    
    // MARK: NSURLSESSION DELEGATES
    
    func URLSession(_ session: Foundation.URLSession, dataTask: URLSessionDataTask, didReceiveData data: Data) {
        
        self.accumulatedWebData!.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

        if let sessionTaskError = error {
            
            print("sessionTaskError \(sessionTaskError)")
            
        } else {
            
            if let dataString = String(data:self.accumulatedWebData! as Data, encoding: String.Encoding.utf8) {

                do {
                    try replaceYoutubeLink(dataString)
                    
                    DispatchQueue.main.async{
                        self.delegate?.youtubeMP3DotOrgModifiedHTMLWebDataAvailable(self)
                    }
                    
                } catch {
                    DispatchQueue.main.async{
                        self.delegate?.youtubeMP3DotOrgFailedToProcessWebData(self)
                    }
                }
                
            } else {
                print("webData to string failed")
            }
        }
        
        session.finishTasksAndInvalidate()
    }

    
    // MARK: - Privates API
    
    // replaceYoutubeLink
    
    //        <div id="form">
    //        <form method="get" action="/" id="submit-form">
    //        <input type="text" id="youtube-url" value="http://www.youtube.com/watch?v=KMU0tzLwhbE" onclick="sAll(this)" autocomplete="off" />
    //            <div id="btns">
    //        <input type="submit" id="submit" value="Convert Video" disabled="disabled" />
    //            </div>
    //        </form>
    //        </div>
    
    enum ReplaceLinkError: Error {
        case linkNotFound
    }
    
    fileprivate func replaceYoutubeLink(_ stringToSearch:String) throws -> Void {
        
        let linkToReplace = "http://www.youtube.com/watch?v=KMU0tzLwhbE"
        
        print(stringToSearch)
        
        if let _ = stringToSearch.range(of: linkToReplace) {

            if let replaceWithString = youTubeURLReplaceString {
                
                youTubeLinkURLStr = replaceWithString
                let newHTML = stringToSearch.replacingOccurrences(of: linkToReplace, with: replaceWithString)
                
                modifiedHTMLWebData = newHTML.data(using: String.Encoding.utf8)
            }
            
        } else {
            throw ReplaceLinkError.linkNotFound
        }
    }

}

// CLASS


