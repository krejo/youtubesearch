//
//  JWYoutubeMP3DotOrgTests.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/2/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import XCTest

@testable import YoutubeSearch

class JWYoutubeMP3DotOrgTests: XCTestCase,
JWYoutubeMP3DotOrgDelegate

{
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    

    // MARK: delegate impl conformance
    
    func youtubeMP3DotOrg(_ controller:JWYoutubeMP3DotOrg, didObtainLink linkString:String){
    }
    func youtubeMP3DotOrgModifiedHTMLWebDataAvailable(_ controller:JWYoutubeMP3DotOrg){
    }
    func youtubeMP3DotOrgFailedToProcessWebData(_ controller:JWYoutubeMP3DotOrg) {
    }
    

    // MARK: -
    
    func testInit() {
        
        let urlSession = JWYoutubeMP3DotOrg()
        
        XCTAssertNotNil(urlSession)
        
        urlSession.delegate = self
        urlSession.youTubeURLReplaceString = "replaceString"
        urlSession.dbkey = "key"
        
        XCTAssertEqual("key", urlSession.dbkey, "dbkey same")
        XCTAssertEqual("replaceString", urlSession.youTubeURLReplaceString, "dbkey same")

    }
    
    func testInitWithParams() {
        
        let urlSession = JWYoutubeMP3DotOrg(replaceString: "replaceString",delegate: self, dbkey: "key")
        
        XCTAssertNotNil(urlSession)
        XCTAssertEqual("key", urlSession.dbkey, "dbkey same")
        XCTAssertEqual("replaceString", urlSession.youTubeURLReplaceString, "dbkey same")
    }

    
}
