//
//  JWYoutubeTests.swift
//  YoutubeSearch
//
//  Created by JOSEPH KERR on 7/3/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import XCTest

@testable import YoutubeSearch

class JWYoutubeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLinkRecord() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let linkRecord = Link(dbkey: "key", linkstr: "http://hello")

        XCTAssertNotNil(linkRecord)
        XCTAssertEqual("key", linkRecord.dbkey, "dbkey same")
        XCTAssertEqual("http://hello", linkRecord.linkstr, "link same")

    }

    
//    func testMP3Record() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//        
//        let mp3Record = MP3DataRecord(dbkey: "key", downloadlinkstr: "http://hello",creationdate: [NSDate date])
//        
//        XCTAssertNotNil(linkRecord)
//        XCTAssertEqual("key", linkRecord.dbkey, "dbkey same")
//        XCTAssertEqual("http://hello", linkRecord.linkstr, "link same")
//        
//    }

    
}
