//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


//: Youtube videos in Array

struct YoutubeVideo {
    var title: String?
    var description: String?
    var videoid: String?
    var thumbURLString: String
}
//struct YoutubeVideo {
//    var title: String
//    var description: String
//    var videoid: String
//}

var searchResults: [YoutubeVideo]?

let v1 = YoutubeVideo(title: "somevideo", description: "cool video", videoid: "v1", thumbURLString: "")

searchResults = [YoutubeVideo]()

searchResults?.append(v1)

//searchResults? += [v1]


if searchResults?.count > 0 {
    print("non zero")
} else {
    print("empty")
}

//: Check Equatable
extension YoutubeVideo: Equatable { }

func ==(lhs:YoutubeVideo, rhs: YoutubeVideo) -> Bool {
    let equals = lhs.title == rhs.title &&
    lhs.description == rhs.description &&
    lhs.videoid == rhs.videoid
    
    return equals
}


let vcheck1 = YoutubeVideo(title: "somevideo", description: "cool video", videoid: "v1", thumbURLString: "")
let vcheck2 = YoutubeVideo(title: "somevideo", description: "cool video", videoid: "v1", thumbURLString: "ssfadf")


if vcheck1 == vcheck2 {
    print("same")
} else {
    print("not same")
}






